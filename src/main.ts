import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { prodVariables } from './environments/environment';

if (prodVariables.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);
