export const prodVariables = {
  // apiEndpoint: ' https://uniserve.herokuapp.com',
  // apiEndpoint: 'http://ec2-52-14-194-189.us-east-2.compute.amazonaws.com',
  // apiEndpoint: ' http://localhost:1337',
  // apiEndpoint: 'https://api.uniserveonline.in',
  // apiEndpoint: 'http://3.6.118.188',
  apiEndpoint: 'https://server.doortask.com',
  environmentName: 'Production Environment',
  ionicEnvName: 'prod',
  production: false
};