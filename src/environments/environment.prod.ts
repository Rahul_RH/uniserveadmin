export const prodVariables = {
  // apiEndpoint: ' https://uniserve.herokuapp.com',
  // apiEndpoint: ' http://localhost:1337',
  // apiEndpoint: 'http://ec2-52-14-194-189.us-east-2.compute.amazonaws.com',
  // apiEndpoint: 'https://api.uniserveonline.in',
  environmentName: 'Production Environment',
  // apiEndpoint: 'http://3.6.118.188',
  apiEndpoint: 'https://server.doortask.com',
  ionicEnvName: 'prod',
  production: false
};