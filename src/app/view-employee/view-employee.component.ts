import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { ServiceExpertsService, ServiceRequestService, ServicesService } from 'app/shared';
import { Validators, FormBuilder, FormGroup, Validator } from '@angular/forms';
import { ToasterService } from 'app/shared/services/toaster-service.service';
import { UserService } from '../shared/services/user.service';

@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.component.html',
  styleUrls: ['./view-employee.component.css']
})
export class ViewEmployeeComponent implements OnInit {

  public allServiceExperts: any;
  public serviceExpertsDetails: any;
  public userProfileForm: FormGroup;
  public userProfileView: FormGroup;
  public userID: any;
  public userDetails: any;
  public dateOfBirth: any;
  public dateOfView: any;
  public Others: any = false;
  public expertServices: any = [];
  public expertsProfile: any;
  public expertStatus: any = [];
  public availableStatus: string;
  public toggleViewAndEdit = true;
  butDisabled = true;
  public expertsStatus: string;
  public loading = false;
  private currentAdmin: any;
  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute,
    private router: Router, private serviceExpertsService: ServiceExpertsService,
    private serviceRequestService: ServiceRequestService, private servicesService: ServicesService,
    private toasterService: ToasterService, private userService: UserService) {

    this.userID = this.route.snapshot.paramMap.get('id');
    this.userProfileForm = this.formBuilder.group({
      firstName: [null, Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z]+$')])],
      lastName: [null, Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z]+$')])],
      mobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^(0|[1-9][0-9]*)$')])],
      address: [null, Validators.compose([Validators.required])],
      expertIn: [null, Validators.compose([Validators.required])],
      availableStatus: [null, Validators.compose([Validators.required])],

    });

    this.userProfileView = this.formBuilder.group({
      firstName: [null, Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z]+$')])],
      lastName: [null, Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z]+$')])],
      mobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^(0|[1-9][0-9]*)$')])],
      address: [null, Validators.compose([Validators.required])],
      expertIn: [null, Validators.compose([Validators.required])],
      availableStatus: [null, Validators.compose([Validators.required])],

    });

    this.userProfileForm.controls['expertIn'].valueChanges.subscribe((value) => {
      if (value == 'Other') {
        this.Others = true;
      }
      else {
        this.Others = false;
      }
    });
  }

  Success() { this.toasterService.Success("Data updated Successfully!!") }
  Error() { this.toasterService.Error("Something Went Wrong!!") }
  Info() { this.toasterService.Info("Data Deleted Successfully!!") }

  ngOnInit() {
    this.userService.currentUser.subscribe(data => this.currentAdmin = data);
    this.serviceExpertsService.allServiceEmployeeData.subscribe(data => {
      this.allServiceExperts = data;
      for (let i = 0; i < this.allServiceExperts.length; i++) {
        if (this.userID == this.allServiceExperts[i].id) {
          this.serviceExpertsDetails = this.allServiceExperts[i];
          this.expertsStatus = this.allServiceExperts[i].availableStatus;
          this.availableStatus = this.allServiceExperts[i].availableStatus;
          this.userProfileForm.patchValue({ firstName: this.allServiceExperts[i].firstName });
          this.userProfileForm.patchValue({ lastName: this.allServiceExperts[i].lastName });
          this.userProfileForm.patchValue({ mobileNumber: this.allServiceExperts[i].mobileNumber });
          this.userProfileForm.patchValue({ address: this.allServiceExperts[i].address });
          this.userProfileForm.patchValue({ expertIn: this.allServiceExperts[i].expertIn });
          this.userProfileForm.patchValue({ availableStatus: this.allServiceExperts[i].availableStatus });
          if (this.allServiceExperts[i].expertIn == 'Other') {
            this.Others = "true";
          }



          this.userProfileView.patchValue({ firstName: this.allServiceExperts[i].firstName })
          this.userProfileView.patchValue({ lastName: this.allServiceExperts[i].lastName });
          this.userProfileView.patchValue({ mobileNumber: this.allServiceExperts[i].mobileNumber });
          this.userProfileView.patchValue({ address: this.allServiceExperts[i].address });
          this.userProfileView.patchValue({ expertIn: this.allServiceExperts[i].expertIn });
          this.userProfileView.patchValue({ availableStatus: this.allServiceExperts[i].availableStatus });
          if (this.allServiceExperts[i].expertIn == 'Other') {
            this.Others = "true";
          }
        }
      }
    })

    this.expertServices.push("Other")
    // this.serviceData = this.servicesService.getServices();
    this.servicesService.currentService.subscribe(serviceData => {
      for (let key in serviceData.success[0]) {
        if (key != 'createdAt' && key != 'updatedAt' && key != 'id') {
          for (let i = 0; i < serviceData.success[0][key].length; i++) {
            this.expertServices.push(serviceData.success[0][key][i]);
          }
        }
      }
    })
    this.expertStatus.push("Available", "Assigned");
  }

  updateExpertProfile() {
    this.loading = true;
    let expertIn;
    let otherExpertizedFields;
    if (this.userProfileForm.value.expertIn == 'Other') {
      otherExpertizedFields = this.userProfileForm.value.otherExpertizedFields
      expertIn = this.userProfileForm.value.expertIn
    }
    else {
      expertIn = this.userProfileForm.value.expertIn
      otherExpertizedFields = ""
    }
    this.expertsProfile = {
      "expertId": this.userID,
      "firstName": this.userProfileForm.value.firstName,
      "lastName": this.userProfileForm.value.lastName,
      "mobileNumber": this.userProfileForm.value.mobileNumber,
      "address": this.userProfileForm.value.address,
      "expertIn": expertIn,
      "availableStatus": this.userProfileForm.value.availableStatus,
      'admin_id': this.currentAdmin.id
    };

    this.serviceExpertsService.updateServiceEmployee(this.expertsProfile).subscribe(data => {
      this.loading = false;
      this.router.navigate(['/serviceemployeedashboard']);
      this.Success();
    },
      error => (this.Error()))
  }

  EditMode() {
    if (this.toggleViewAndEdit == true) {
      this.toggleViewAndEdit = false;
    }
    else {
      this.toggleViewAndEdit = true;
    }
  }

  delete() {
    const action = confirm("Are You Sure To Delete....?")
    if (action) {
      this.loading = true;
      var expertsData = { "id": this.userID, 'admin_id': this.currentAdmin.id }
      if (this.expertsStatus == 'Available') {
        this.serviceExpertsService.deleteServiceEmployee(expertsData).subscribe(data => {
          this.loading = false;
          this.router.navigate(['/serviceemployeedashboard']);
          this.Info();
        },
          error => (this.Error()))
      }
      else {
        this.loading = false;
        alert("can not delete the profile")
      }
    }

  }

  cancel() {
    this.router.navigate(['/serviceemployeedashboard']);
  }


}
