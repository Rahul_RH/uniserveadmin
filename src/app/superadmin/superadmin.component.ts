import { Component, OnInit } from '@angular/core';
import { ServiceExpertsService } from '../shared/services';

@Component({
  selector: 'app-superadmin',
  templateUrl: './superadmin.component.html',
  styleUrls: ['./superadmin.component.css']
})
export class SuperadminComponent implements OnInit {

  allAdminList: any;
  allChannelPartenerList: any;
  allVendorsList: any;
  constructor(private serviceExpertsService: ServiceExpertsService) { }

  ngOnInit() {
    this.serviceExpertsService.getAllAdmins().subscribe((response: any) => {
      this.allAdminList = response.success;
      // console.log(this.allAdminList)
    })

    this.serviceExpertsService.getAllChannelPartener().subscribe((response: any) => {
      this.allChannelPartenerList = response.success;
    })

    this.serviceExpertsService.getAllVendorsSuperAdmin().subscribe((response: any) => {
      console.log(response)
      this.allVendorsList = response.success;
    })


  }

}
