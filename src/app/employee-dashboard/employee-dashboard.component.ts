import { Component, OnInit } from '@angular/core';
import { ServiceExpertsService } from '../shared/services';

@Component({
  selector: 'app-employee-dashboard',
  templateUrl: './employee-dashboard.component.html',
  styleUrls: ['./employee-dashboard.component.css']
})
export class EmployeeDashboardComponent implements OnInit {
  public allServiceExperts: any = [];
  constructor(private serviceExpertsService: ServiceExpertsService) { }

  ngOnInit() {
    this.serviceExpertsService.allServiceEmployeeData.subscribe(data => {
      console.log(data)
      this.allServiceExperts = data;
    })
  }

}
