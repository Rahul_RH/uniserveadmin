import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FileValidator } from '../shared/validator/file-input.validator';
import { ServicesService, ServiceExpertsService, ServiceRequestService } from '../shared';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../shared/services/user.service';
import { ToasterService } from 'app/shared/services/toaster-service.service';

@Component({
  selector: 'app-create-vendors',
  templateUrl: './create-vendors.component.html',
  styleUrls: ['./create-vendors.component.css']
})
export class CreateVendorsComponent implements OnInit {

  public userProfileForm: FormGroup;
  // private firstName: string = "";
  // private lastName: string = "";
  // private mobile: number;
  public todayDate: any;
  public files: any;
  public expertsProfile: any;
  public ecodedFile: any = [];
  public Others: any = false;
  public loading = false;
  currentUserDetails: any;
  // private address: string = "";
  // private adharNumber: string = ""; 
  // private ExpertIn: string = "";
  // private workExperience: DoubleRange;
  // private additionalInfo: string = "";
  // private gender: string = "";
  public serviceData: any;
  public expertServices: any = [];
  // ,Validators.pattern('^[0-9]{5}(?:-[0-9]{4})?$')
  constructor(
    private formBuilder: FormBuilder,
    private servicesService: ServicesService,
    private serviceExpertsService: ServiceExpertsService,
    private router: Router,
    private toasterService: ToasterService,
    private userService: UserService) {
    this.todayDate = new Date().toISOString().substring(0, 10);

    this.userProfileForm = this.formBuilder.group({
      firstName: [null, Validators.compose([Validators.required])],
      lastName: [null, Validators.compose([Validators.required])],
      mobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^(0|[1-9][0-9]*)$')])],
      dateOfBirth: [null],
      address: [null, Validators.compose([Validators.required])],
      adharNumber: [null, Validators.compose([Validators.pattern('^(0|[1-9][0-9]*)$'), Validators.minLength(12), Validators.maxLength(12)])],
      expertIn: [null, Validators.compose([Validators.required])],
      workExperience: [null, Validators.compose([Validators.pattern('^[0-9]+(\.[0-9]{1,2})?$')])],
      additionalInfo: [null],
      gender: ["Male"],
      otherExpertizedFields: [null],
      file: ["", Validators.compose([FileValidator.validate])]
    });

    this.userProfileForm.controls['expertIn'].valueChanges.subscribe((value) => {
      if (value == 'Other') {
        this.Others = true;
      }
      else {
        this.Others = false;
      }
    });

  }

  ngOnInit() {
    this.currentUserDetails = this.userService.getCurrentUser()
    this.expertServices.push("Other")
    // this.serviceData = this.servicesService.getServices();
    this.servicesService.currentService.subscribe(serviceData => {
      for (let key in serviceData.success[0]) {
        if (key != 'createdAt' && key != 'updatedAt' && key != 'id') {
          for (let i = 0; i < serviceData.success[0][key].length; i++) {
            this.expertServices.push(serviceData.success[0][key][i]);
          }
        }
      }
    })
  }

  saveExpertProfile() {
    this.loading = true;
    let expertIn;
    let otherExpertizedFields;
    if (this.userProfileForm.value.expertIn == 'Other') {
      otherExpertizedFields = this.userProfileForm.value.otherExpertizedFields
      expertIn = this.userProfileForm.value.expertIn
    }
    else {
      expertIn = this.userProfileForm.value.expertIn
    }
    this.expertsProfile = {
      "firstName": this.userProfileForm.value.firstName,
      "lastName": this.userProfileForm.value.lastName,
      "mobileNumber": this.userProfileForm.value.mobileNumber,
      "dateOfBirth": this.todayDate,
      "gender": this.userProfileForm.value.gender,
      "address": this.userProfileForm.value.address,
      "adharNumber": this.userProfileForm.value.adharNumber,
      "expertIn": expertIn,
      "workExperience": this.userProfileForm.value.workExperience,
      "additionalInfo": this.userProfileForm.value.additionalInfo,
      "files": this.ecodedFile,
      "otherExpertizedFields": otherExpertizedFields,
      "availableStatus": "Available",
      "admin_id": this.currentUserDetails.id,
    };

    // this.serviceExpertsService.addServiceExperts( this.expertsProfile).subscribe(data => {
    //   this.loading = false;
    //   this.router.navigate(['/dashboard']);
    // })
    this.serviceExpertsService.addVendor(this.expertsProfile).subscribe(data => {
      this.loading = false;
      this.router.navigate(['/vendorsdashboard']);
      this.Success();
    },
      error => (this.Error())
    )

  }

  fileChange(event) {
    this.ecodedFile = [];
    this.files = event.srcElement.files;
    if (this.files && this.files.length > 0) {
      for (let i = 0; i < this.files.length; i++) {
        let reader = new FileReader();
        reader.readAsDataURL(this.files[i]);
        reader.onload = () => {
          this.ecodedFile.push(reader.result)
        };
      }
    }
  }




  // public loading = false;
  // public userProfileForm: FormGroup;
  // public employeeProfile: any;
  // todayDate: string;
  // public expertServices: any = [];
  // currentUserDetails: any;
  // constructor(private formBuilder: FormBuilder,
  //   private toasterService:ToasterService,
  //   private servicesService: ServicesService,
  //   private serviceExpertsService: ServiceExpertsService,
  //   private userService: UserService,
  //   private router: Router) { }

  Success() { this.toasterService.Success("Vendor Data Added Successfully!!") }
  Error() { this.toasterService.Error("Something Went Wrong!!") }

  // ngOnInit() {
  //   this.currentUserDetails = this.userService.getCurrentUser()

  //   this.todayDate = new Date().toISOString().substring(0, 10);
  //   this.userProfileForm = this.formBuilder.group({
  //     firstName: [null, Validators.compose([Validators.required])],
  //     lastName: [null, Validators.compose([Validators.required])],
  //     mobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^(0|[1-9][0-9]*)$')])],
  //     address: [null, Validators.compose([Validators.required])],
  //     expertIn: [null, Validators.compose([Validators.required])]
  //   });

  //   this.expertServices.push("Other")
  //   // this.serviceData = this.servicesService.getServices();
  //   this.servicesService.currentService.subscribe(serviceData => {
  //     for (let key in serviceData.success[0]) {
  //       if (key != 'createdAt' && key != 'updatedAt' && key != 'id') {
  //         for (let i = 0; i < serviceData.success[0][key].length; i++) {
  //           this.expertServices.push(serviceData.success[0][key][i]);
  //         }
  //       }
  //     }
  //   })
  // }

  // saveExpertProfile() {
  //   this.loading = true;
  //   let expertIn;
  //   let otherExpertizedFields;
  //   if (this.userProfileForm.value.expertIn == 'Other') {
  //     otherExpertizedFields = this.userProfileForm.value.otherExpertizedFields
  //     expertIn = this.userProfileForm.value.expertIn
  //   }
  //   else {
  //     expertIn = this.userProfileForm.value.expertIn
  //   }
  //   this.employeeProfile = {
  //     "firstName": this.userProfileForm.value.firstName,
  //     "lastName": this.userProfileForm.value.lastName,
  //     "mobileNumber": this.userProfileForm.value.mobileNumber,
  //     "address": this.userProfileForm.value.address,
  //     "expertIn": expertIn,
  //     "admin_id": this.currentUserDetails.id,
  //     "availableStatus": "Available"
  //   };
  //   // console.log("test", this.employeeProfile)
  //   this.serviceExpertsService.addVendor(this.employeeProfile).subscribe(data => {
  //     this.loading = false;
  //     this.router.navigate(['/vendorsdashboard']);
  //     this.Success();
  //   },
  //   error=>(this.Error())
  //   )
  // }

}
