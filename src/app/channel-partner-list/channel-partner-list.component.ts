import { Component, OnInit } from '@angular/core';
import { ServiceExpertsService } from '../shared';

@Component({
  selector: 'app-channel-partner-list',
  templateUrl: './channel-partner-list.component.html',
  styleUrls: ['./channel-partner-list.component.css']
})
export class ChannelPartnerListComponent implements OnInit {

  allChannelPartenerList: any;
  constructor(private serviceExpertsService: ServiceExpertsService) { }

  ngOnInit() {
    this.serviceExpertsService.getAllChannelPartener().subscribe((response: any) => {
      this.allChannelPartenerList = response.success;
    })
  }
}
