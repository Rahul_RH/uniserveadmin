import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelPartnerListComponent } from './channel-partner-list.component';

describe('ChannelPartnerListComponent', () => {
  let component: ChannelPartnerListComponent;
  let fixture: ComponentFixture<ChannelPartnerListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChannelPartnerListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelPartnerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
