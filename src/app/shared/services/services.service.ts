import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { ReplaySubject } from "rxjs/ReplaySubject";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';
import { ApiService } from './api.service';

@Injectable()
export class ServicesService {
    public RequestOptions: any;
    private serveyData: any;

    private currentServiceSubject = new ReplaySubject<any>();
public currentService = this.currentServiceSubject.asObservable();

    private currentUserRequest = new BehaviorSubject<Object>(1);
    public UserRequest = this.currentUserRequest.asObservable().distinctUntilChanged();

    constructor(
        private apiService: ApiService
    ) { }
    
    getServicesAPI() {
        this.apiService.post('/services/getServices')
            .subscribe(data => {
                this.setServices(data);

            })
    }
    setServices(data: any) {
        this.currentServiceSubject.next(data);
    }
  
    getRequestOptionsAPI() {
        return this.apiService.post('/RequestOptions/getRequestOptions')
            .map(data => {
                this.setRequestOptions(data);
                return data;
            })
    }
    setRequestOptions(data: any) {
        this.RequestOptions = data;
    }
    getRequestOptions(): any {
        return this.RequestOptions;
    }
    sendRequest(data: any) {
        return this.apiService.post('/UserRequestServices/createRequest', data)
            .map(data => {
                this.setCurrentUserRequest(data.success);
                return data;
            })
    }
    getUserRequestAPI(data): any {
        return this.apiService.post('/UserRequestServices/getUserRequest', data)
            .subscribe(data => {
                this.setCurrentUserRequest(data.success);
            })
    }

    setCurrentUserRequest(requestInfo: any) {
        this.currentUserRequest.next(requestInfo);
    }
    getCurrentUserRequest(): any {
        return this.currentUserRequest.value;
    }

    setServeyData(data: any) {
      
        this.serveyData = data;
    }
    getServeyData() {
        return this.serveyData;
    }

    getExpertReport(data){
        return this.apiService.post('/expertsReport/getExpertReport', data)
            .map(data => {
                return data;
            })
    }
}