import { Injectable } from '@angular/core';
declare var toastr:any
@Injectable()
export class ToasterService {

  constructor() {
    this.setting()
   }
 Success(title:string,message?:string){
  toastr.success(title,message);
 }
 warning(title:string,message?:string)
 {
   toastr.warning(message,title)
 }
 Error(title:string,message?:string)
 {
   toastr.error(message,title)
 }
 Error1(title:string,message?:string)
 {
   toastr.error(message,title)
 }
 Info(title:string,message?:string)
 {
   toastr.info(message,title)
 }
 setting(){
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-full-width",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
 }

}
