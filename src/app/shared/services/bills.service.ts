import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ApiService } from '.';


@Injectable()
export class BillsService {

  private allBillSubject = new BehaviorSubject<Array<Object>>(new Array());
  public allBills = this.allBillSubject.asObservable().distinctUntilChanged();

  constructor(private apiService: ApiService) { }

  setAllBills(dataObj: any){
    this.allBillSubject.next(dataObj);
  }

  fetchAllBillAPI(): any {
    this.apiService.get('/getAllBill').subscribe(data =>{
      this.setAllBills(data);
    })
  }

  generatebill(data): any{
    return this.apiService.post('/genearateBill', data).map(data =>{
      console.log(data);
      this.setAllBills(data);
      return data;
    })
  }
}
