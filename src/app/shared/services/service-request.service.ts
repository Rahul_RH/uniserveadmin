import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ServiceExpertsService } from './service-experts.service';
import { ReplaySubject } from "rxjs/ReplaySubject";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';
@Injectable()
export class ServiceRequestService {



  private allUserRequestSubject = new BehaviorSubject<Object>(new Object());
  public allUserRequest = this.allUserRequestSubject.asObservable();

  private newUserRequestSubject = new ReplaySubject<any>();
  public newUserRequest = this.newUserRequestSubject.asObservable();

  constructor(private ApiService: ApiService, private serviceExpertsService: ServiceExpertsService) {

  }

  fetchAllUserRequestsAPI(body): any {
    this.ApiService.post('/UserRequestServices/getAllUserRequest', body).subscribe(data => {

      this.setAllUsersRequests(data);


    })
  }

  fetchAllSuperAdminServiceRequest(): any {
    return this.ApiService.get('/UserRequestServices/getAllRequestsForSuperAdmin')
  }

  setAllUsersRequests(data): any {
    this.allUserRequestSubject.next(data.success);
  }

  getAllUsersRequests(): any {
    // return this.allUserRequest.success;
  }

  // getAllUserRequestPromise():any{
  //   let promise = new Promise((resolve, reject) => {
  //     if(this.allUserRequest){
  //       resolve(this.allUserRequest.success);
  //     }
  //     else{
  //       resolve(undefined)
  //     }

  //   });
  //   return promise;
  // }

  fetchNewUserRequestAPI(): any {
    this.ApiService.get('/UserRequestServices/getNewUserRequest').subscribe(data => {
      this.setNewUserRequest(data);
    })
  }
  setNewUserRequest(data): any {
    this.newUserRequestSubject.next(data);
  }
  // getNewUserRequest(): any{
  // return this.newUserRequest.success;
  // }setAllServiceExpertsData


  assignServiceExpert(data: any): any {
    return this.ApiService.post('/UserRequestServices/assignServiceExpert', data).map(responseData => {
      this.allUserRequestSubject.next(responseData.userRequestedData);
      this.serviceExpertsService.setAllServiceExpertsData(responseData.expertsData)
      return responseData;
    })
  }

  updateRequestStatus(data: any): any {
    return this.ApiService.post('/UserRequestServices/updateRequestStatus', data).map(responseData => {
      this.allUserRequestSubject.next(responseData.responseData);
      if (responseData && responseData.serviceData) {
        this.serviceExpertsService.setAllServiceExpertsData(responseData.serviceData)
      }
      return responseData;
    })
  }
  cancelRequest(data: any): any {
    return this.ApiService.post('/UserRequestServices/cancelServiceRequestAdmin', data).map(responseData => {
      this.allUserRequestSubject.next(responseData);
      return responseData;
    })
  }

  updateRequestExpert(data: any): any {
    return this.ApiService.post('/UserRequestServices/updateServiceExpert', data).map(responseData => {
      if (responseData.success) {
        this.allUserRequestSubject.next(responseData.success);
      }
      return responseData;
    })
  }
  updateRequestTimings(data: any): any {
    return this.ApiService.post('/UserRequestServices/updateServiceDateAndTime', data).map(responseData => {
      if (responseData.success) {
        this.allUserRequestSubject.next(responseData.success);
      }
      return responseData;
    })
  }

  updateRequestNote(data: any): any {
    return this.ApiService.post('/UserRequestServices/updateRequestNote', data).map(responseData => {
      if (responseData.success) {
        this.allUserRequestSubject.next(responseData.success);
      }
      return responseData;
    })
  }

  updateRequestAmount(data: any): any {
    return this.ApiService.post('/UserRequestServices/updateRequestAmount', data).map(responseData => {
      if (responseData.success) {
        this.allUserRequestSubject.next(responseData.success);
      }
      return responseData;
    })
  }

}
