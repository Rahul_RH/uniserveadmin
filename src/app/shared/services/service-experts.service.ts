import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Console } from '@angular/core/src/console';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ServiceExpertsService {

  private allServiceExpertsDataSubject = new ReplaySubject<any>();
  public allServiceExpertsData = this.allServiceExpertsDataSubject.asObservable();

  private allServiceEmployeeDataSubject = new ReplaySubject<any>();
  public allServiceEmployeeData = this.allServiceEmployeeDataSubject.asObservable();

  constructor(private apiService: ApiService,
  ) { }

  private formatErrors(error: any) {
    return Observable.throw(error.json());
  }
  addServiceExperts(data: any): any {
    return this.apiService.post('/serviceExperts/addServiceExpert', data).map(responseData => {
      this.setAllServiceExpertsData(responseData);
      return responseData;
    })
  }
  addServiceEmployee(data: any): any {
    return this.apiService.post('/serviceExperts/addServiceEmployee', data).map(responseData => {
      this.setAllServiceEmployeeData(responseData);
      return responseData;
    })
  }
  addAdmin(data: any): any {
    return this.apiService.post('/admin/adminSignup', data).map(responseData => {
      return responseData;
    })
  }
  getAllAdmins(): any {
    return this.apiService.get('/admin/getAllAdmins').map(responseData => {
      return responseData;
    })
  }
  addChannelPartener(data: any): any {
    return this.apiService.post('/channelpartener/createChannelPartener', data).map(responseData => {
      return responseData;
    })
  }
  getAllChannelPartener(): any {
    return this.apiService.get('/channelpartener/getAllChannelPartener').map(responseData => {
      return responseData;
    })
  }
  addVendor(data: any): any {
    return this.apiService.post('/vendors/createVendors', data).map(responseData => {
      return responseData;
    })
  }
  getAllVendors(data: any): any {
    return this.apiService.post('/vendors/getAllVendors', data).map(responseData => {
      return responseData;
    })
  }
  updateVendors(data: any): any {
    return this.apiService.post('/vendors/updateVendorDetails', data).map(responseData => {
      return responseData;
    })
  }
  deleteVendors(data: any): any {
    return this.apiService.post('/vendors/deleteVendorDetails', data).map(responseData => {
      return responseData;
    })
  }
  getAllVendorsSuperAdmin(): any {
    return this.apiService.get('/vendors/getAllVendorsSuperAdmin').map(responseData => {
      return responseData;
    })
  }
  updateServiceExperts(data: any): any {
    return this.apiService.post('/serviceExperts/updateExpertDetails', data).map(responseData => {
      this.setAllServiceExpertsData(responseData);
      return responseData;
    })
  }
  updateServiceEmployee(data: any): any {
    return this.apiService.post('/serviceExperts/updateEmployeeDetails', data).map(responseData => {
      this.setAllServiceEmployeeData(responseData);
      return responseData;
    })
  }
  deleteServiceExperts(data: any): any {
    console.log(data);
    return this.apiService.post('/serviceExperts/deleteExpertDetails', data).map(responseData => {
      this.setAllServiceExpertsData(responseData);
      return responseData;
    })
  }
  deleteServiceEmployee(data: any): any {
    return this.apiService.post('/serviceExperts/deleteEmployeeDetails', data).map(responseData => {
      this.setAllServiceEmployeeData(responseData);
      return responseData;
    })
  }
  getAllServiceExpertsAPI(): any {
    this.apiService.get('/serviceExperts/getAllServiceExpert').subscribe(response => {
      this.setAllServiceExpertsData(response)
    })
  }
  getAllServiceEmployeeAPI(admin_id): any {
    let obj = {
      'admin_id': admin_id
    }
    this.apiService.post('/serviceExperts/getAllServiceEmployee', obj).subscribe(response => {
      this.setAllServiceEmployeeData(response)
    })
  }

  setAllServiceExpertsData(data: any) {
    this.allServiceExpertsDataSubject.next(data);
  }

  setAllServiceEmployeeData(data: any) {
    this.allServiceEmployeeDataSubject.next(data);
  }

  // getAllServiceExpertsData(){
  //   return this.allServiceExpertsData;
  // }

  getAllServiceExpertsDataPromise(): any {
    let promise = new Promise((resolve, reject) => {
      if (this.allServiceExpertsData) {
        resolve(this.allServiceExpertsData);
      } else {
        resolve(undefined);
      }

    });
    return promise;
  }
  getAllServiceEmployeeDataPromise(): any {
    let promise = new Promise((resolve, reject) => {
      if (this.allServiceEmployeeData) {
        resolve(this.allServiceEmployeeData);
      }
      else {
        resolve(undefined)
      }

    });
    return promise;
  }
}
