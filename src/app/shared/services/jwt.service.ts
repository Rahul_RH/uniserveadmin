import { Injectable } from '@angular/core';

@Injectable()
export class JwtService {

  constructor() { }

  getToken(): String {
    return window.localStorage['uniserveUserToken'];
  }

  saveToken(token: String) {
    window.localStorage['uniserveUserToken'] = token;
  }

  destroyToken() {
    window.localStorage.removeItem('uniserveUserToken');
  }


}
