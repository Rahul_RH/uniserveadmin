import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ApiService } from './api.service';
import { JwtService } from './jwt.service';
// import { SqlLiteService } from "./sqlite.service";
import { User } from '../models/user.model';
import { ServiceRequestService } from './service-request.service';
import { ServiceExpertsService } from './service-experts.service'
import { ServicesService } from './services.service';
import { BillsService } from './bills.service';
import { Router } from '@angular/router';



@Injectable()
export class UserService {
  private currentUserSubject = new BehaviorSubject<User>(new User());
  public currentUser = this.currentUserSubject.asObservable().distinctUntilChanged();

  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();

  private newUserSubject = new ReplaySubject<any>();
  public newUser = this.newUserSubject.asObservable();


  private allUsersSubject = new ReplaySubject<any>();
  public allUsers = this.allUsersSubject.asObservable();

  private allBillSubject = new ReplaySubject<any>();
  public allBills = this.allBillSubject.asObservable();


  private userDetails: User = new User();
  constructor(
    private apiService: ApiService,
    private jwtService: JwtService,
    private serviceRequestService: ServiceRequestService,
    private serviceExpertsService: ServiceExpertsService,
    private servicesService: ServicesService,
    private billsService: BillsService,
    private router: Router
  ) { }

  populate() {

    // If JWT detected, attempt to get & store user's info
    if (this.jwtService.getToken() != undefined) {
      this.apiService.post('/admin/getUserDetails', { "userId": this.jwtService.getToken() })
        .subscribe(
          data => {
            if (data.success) {
              this.userDetails.id = data.success[0].id;
              this.userDetails.usertype = data.success[0].usertype;
              if (data.success[0].contactDetails.email) {
                this.userDetails.contactdetails.email = data.success[0].contactDetails.email;
              }
              if (data.success[0].contactDetails.mobile) {
                this.userDetails.contactdetails.mobile = data.success[0].contactDetails.mobile;
              }
              // if (this.userDetails.usertype == "superadmin") {
              //   this.router.navigate(['superadmin'])
              // }
              this.setAuth(this.userDetails);
              this.serviceRequestService.fetchAllUserRequestsAPI({ "admin_id": this.userDetails.id })
              setInterval(() => this.serviceRequestService.fetchAllUserRequestsAPI({ "admin_id": this.userDetails.id }), 10000)

              this.serviceRequestService.fetchNewUserRequestAPI();
              // this.serviceExpertsService.getAllServiceExpertsAPI();
              this.serviceExpertsService.getAllServiceEmployeeAPI(this.userDetails.id);
              this.servicesService.getServicesAPI();
              this.billsService.fetchAllBillAPI();
              this.fetchNewUserAPI();
              this.fetchAllUsersAPI()
            }
            else {
              this.purgeAuth()
            }
          },
          err => this.purgeAuth()
        );
    } else {
      // Remove any potential remnants of previous auth states
      this.purgeAuth();
    }
  }

  setAuth(user: User) {
    // Save JWT sent from server in localstorage
    this.jwtService.saveToken(user.id);
    // Set current user data into observable
    this.currentUserSubject.next(user);
    // Set isAuthenticated to true
    this.isAuthenticatedSubject.next(true);
  }

  purgeAuth() {
    // Remove JWT from localstorage
    this.jwtService.destroyToken();
    // Set current user to an empty object
    this.currentUserSubject.next(new User());
    // Set auth status to false
    this.isAuthenticatedSubject.next(false);
  }

  attemptAuth(type, credentials): Observable<User> {
    const route = (type === 'login') ? '/login' : '';
    return this.apiService.post('/users' + route, { user: credentials })
      .map(
        data => {
          this.setAuth(data.user);
          return data;
        }
      );
  }
  // Update the user on the server (email, pass, etc)
  update(user): Observable<User> {
    return this.apiService
      .put('/user', { user })
      .map(data => {
        // Update the currentUser observable
        this.currentUserSubject.next(data.user);
        return data.user;
      });
  }


  login(data: Object = {}) {
    return this.apiService.post('/admin/adminLogin', data)
      .map(data => {
        if (data.success) {
          this.userDetails.id = data.success.id;
          this.userDetails.usertype = data.success.usertype;
          if (data.success.contactDetails.email) {
            this.userDetails.contactdetails.email = data.success.contactDetails.email;
          }
          if (data.success.contactDetails.mobile) {
            this.userDetails.contactdetails.mobile = data.success.contactDetails.mobile;
          }

          this.setAuth(this.userDetails);
        }
        return data;
      })
  }
  updateProfile(data) {
    return this.apiService.post('/user/updateProfile', data)
      .map(data => {
        return data;
      })
  }
  addAddress(data) {
    return this.apiService.post('/user/addAddress', data)
      .map(data => {
        return data;
      })
  }
  verifyOTP(data: Object = {}) {
    return this.apiService.post('/user/verifyOTP', data)
      .map(data => {
        return data;
      })
  }
  logout() {
    // let userDetails = this.getCurrentUser();
    // let userId = userDetails[0].id;
    // this.apiService.post('/user/logout', { 'id': userId }).subscribe(data => { });
  }
  getUserProfile(): any {
    // let userDetails = this.getCurrentUser();
    // let data = { "id": userDetails[0].id }
    // return this.apiService.post('/user/getuserProfile', data)
    //     .map(user => {
    //         this.setCurrentUser(user.success);
    //         return user;
    //     })

  }

  setCurrentUser(user: any) {
    this.currentUserSubject.next(user);
  }
  getCurrentUser(): any {
    return this.currentUserSubject.value;
  }
  getUser(): any {
    let promise = new Promise((resolve, reject) => {

    });
    return promise;
  }
  resendOtp(data: any) {
    this.apiService.post('/user/resendOtp', data).subscribe(data => {

    })
  }
  removeAddress(data: any) {
    return this.apiService.post('/user/removeAddress', data).map(data => {
      return data;
    })
  }

  fetchNewUserAPI(): any {
    this.apiService.get('/user/newUsers').subscribe(data => {
      this.setNewUsers(data);
    })
  }

  setNewUsers(data): any {
    this.newUserSubject.next(data);
  }

  fetchAllUsersAPI(): any {
    this.apiService.get('/user/getAllUsers').subscribe(data => {
      this.setAllUsers(data);

    })
  }

  setAllUsers(data): any {
    this.allUsersSubject.next(data);
  }

}