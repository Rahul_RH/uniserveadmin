import { TestBed, inject } from '@angular/core/testing';

import { ServiceExpertsService } from './service-experts.service';

describe('ServiceExpertsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServiceExpertsService]
    });
  });

  it('should be created', inject([ServiceExpertsService], (service: ServiceExpertsService) => {
    expect(service).toBeTruthy();
  }));
});
