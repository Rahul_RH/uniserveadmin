import { NgModule } from '@angular/core';
import { ValuesPipe } from './ValuesPipe.pipe';
import { SortgridPipe } from './sortgridPipe.pipe';

@NgModule({
    imports: [],
    declarations: [ValuesPipe, SortgridPipe],
    exports: [ValuesPipe, SortgridPipe],
})

export class PipeModule {

    static forRoot() {
        return {
            ngModule: PipeModule,SortgridPipe,
            providers: [],
        };
    }
} 