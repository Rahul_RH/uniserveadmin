export class User {
  email: string;
  token: string;
  username: string;
  bio: string;
  image: string;
  id: string;
  contactdetails: ContactDetails = new ContactDetails();
  usertype: string;
}

export class ContactDetails {
  email: string;
  mobile: number;
}