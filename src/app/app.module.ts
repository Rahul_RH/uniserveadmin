import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
// import { MyDateRangePickerModule } from 'mydaterangepicker';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TableListComponent } from './table-list/table-list.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UpgradeComponent } from './upgrade/upgrade.component';
import { EditRequestComponent } from './edit-request/edit-request.component';
// import { SpinnerModule } from '@chevtek/angular-spinners';
import {
  ApiService,
  ServicesService,
  ServiceRequestService,
  UserService,
  ServiceExpertsService,
  JwtService,
  AuthGuardService,
  BillsService
} from './shared';
import { PipeModule } from './shared/pipes/pipe.module';
import { FileValidator } from './shared/validator/file-input.validator';
import { FileValueAccessor } from './shared/validator/file-control-value-accessor';
import { LoginComponent } from './login/login.component';
import { ServiceExpertComponent } from './service-expert/service-expert.component';
import { NgDatepickerModule } from 'ng2-datepicker';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';
import { GenerateBillComponent } from './generate-bill/generate-bill.component';
import { ExpertsServiceReportComponent } from './experts-service-report/experts-service-report.component';
import { ExpertsDashboardComponent } from './experts-dashboard/experts-dashboard.component';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { EmployeeDashboardComponent } from './employee-dashboard/employee-dashboard.component';
import { ViewEmployeeComponent } from './view-employee/view-employee.component';
import { SuperadminComponent } from './superadmin/superadmin.component';
import { CreateAdminComponent } from './create-admin/create-admin.component';
import { CreateChannelPartenerComponent } from './create-channel-partener/create-channel-partener.component';
import { CreateVendorsComponent } from './create-vendors/create-vendors.component';
import { VendorsDashboardComponent } from './vendors-dashboard/vendors-dashboard.component';
import { SuperAdminServiceRequestsComponent } from './super-admin-service-requests/super-admin-service-requests.component';
import { ToasterService } from './shared/services/toaster-service.service';
import { AdminListComponent } from './admin-list/admin-list.component';
import { ChannelPartnerListComponent } from './channel-partner-list/channel-partner-list.component';
import { ViewAdminComponent } from './view-admin/view-admin.component';
import { ViewChannelPartnerComponent } from './view-channel-partner/view-channel-partner.component';
import { ViewServiceVendorComponent } from './view-service-vendor/view-service-vendor.component';
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    EditRequestComponent,
    FileValidator,
    FileValueAccessor,
    LoginComponent,
    ServiceExpertComponent,
    GenerateBillComponent,
    ExpertsServiceReportComponent,
    ExpertsDashboardComponent,
    CreateEmployeeComponent,
    EmployeeDashboardComponent,
    ViewEmployeeComponent,
    SuperadminComponent,
    CreateAdminComponent,
    CreateChannelPartenerComponent,
    CreateVendorsComponent,
    VendorsDashboardComponent,
    SuperAdminServiceRequestsComponent,
    AdminListComponent,
    ChannelPartnerListComponent,
    ViewAdminComponent,
    ViewChannelPartnerComponent,
    ViewServiceVendorComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    NgDatepickerModule,
    PipeModule.forRoot(),
    AmazingTimePickerModule,
    LoadingModule.forRoot({
      animationType: ANIMATION_TYPES.threeBounce,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)',
      backdropBorderRadius: '4px',
      primaryColour: '#ffffff',
      secondaryColour: '#ffffff',
      tertiaryColour: '#ffffff'
    })
    // MyDateRangePickerModule
  ],
  providers: [ApiService, ServicesService, ServiceRequestService, UserService, ServiceExpertsService, JwtService, AuthGuardService, BillsService, ToasterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
