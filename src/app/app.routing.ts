import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TableListComponent } from './table-list/table-list.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UpgradeComponent } from './upgrade/upgrade.component';
import { EditRequestComponent } from './edit-request/edit-request.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './shared/services/auth-guard.service';
import { ServiceExpertComponent } from './service-expert/service-expert.component';
import { GenerateBillComponent } from './generate-bill/generate-bill.component';
import { ExpertsServiceReportComponent } from './experts-service-report/experts-service-report.component';
import { ExpertsDashboardComponent } from './experts-dashboard/experts-dashboard.component';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { EmployeeDashboardComponent } from './employee-dashboard/employee-dashboard.component';
import { ViewEmployeeComponent } from './view-employee/view-employee.component';
import { SuperadminComponent } from './superadmin/superadmin.component';
import { CreateAdminComponent } from './create-admin/create-admin.component';
import { CreateChannelPartenerComponent } from './create-channel-partener/create-channel-partener.component';
import { CreateVendorsComponent } from './create-vendors/create-vendors.component';
import { VendorsDashboardComponent } from './vendors-dashboard/vendors-dashboard.component';
import { SuperAdminServiceRequestsComponent } from './super-admin-service-requests/super-admin-service-requests.component';
import { AdminListComponent } from './admin-list/admin-list.component';
import { ChannelPartnerListComponent } from './channel-partner-list/channel-partner-list.component';
import { ViewAdminComponent } from './view-admin/view-admin.component';
import { ViewChannelPartnerComponent } from './view-channel-partner/view-channel-partner.component';
import { ViewServiceVendorComponent } from './view-service-vendor/view-service-vendor.component';

// ,canActivate: [AuthGuardService] 
const routes: Routes = [
 
  // { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService] },
  // { path: '', redirectTo: 'servicerequests/:tab', pathMatch: 'full', canActivate: [AuthGuardService] },
  { path: 'addexperts', component: UserProfileComponent, canActivate: [AuthGuardService] },
  { path: 'addemployee', component: CreateEmployeeComponent, canActivate: [AuthGuardService] },
  { path: 'servicerequests/:tab', component: TableListComponent, canActivate: [AuthGuardService] },
  { path: 'assignexpert/:id', component: TypographyComponent, canActivate: [AuthGuardService] },
  { path: 'report', component: IconsComponent, canActivate: [AuthGuardService] },
  { path: 'maps', component: MapsComponent, canActivate: [AuthGuardService] },
  { path: 'notifications', component: NotificationsComponent, canActivate: [AuthGuardService] },
  { path: 'upgrade', component: UpgradeComponent, canActivate: [AuthGuardService] },
  { path: 'edit-request', component: EditRequestComponent, canActivate: [AuthGuardService] },
  { path: 'login', component: LoginComponent },
  { path: 'service-expert/:id', component: ServiceExpertComponent, canActivate: [AuthGuardService] },
  { path: 'service-employee/:id', component: ViewEmployeeComponent, canActivate: [AuthGuardService] },
  { path: 'expertreport/:id', component: ExpertsServiceReportComponent, canActivate: [AuthGuardService] },
  { path: 'edit-request/:id', component: EditRequestComponent, canActivate: [AuthGuardService] },
  { path: 'generatebill/:id', component: GenerateBillComponent, canActivate: [AuthGuardService] },
  { path: 'serviceexpertdashboard', component: ExpertsDashboardComponent, canActivate: [AuthGuardService] },
  { path: 'serviceemployeedashboard', component: EmployeeDashboardComponent, canActivate: [AuthGuardService] },
  { path: 'superadmin', component: SuperadminComponent, canActivate: [AuthGuardService] },
  { path: 'createadmin', component: CreateAdminComponent, canActivate: [AuthGuardService] },
  { path: 'createchannelpartener', component: CreateChannelPartenerComponent, canActivate: [AuthGuardService] },
  { path: 'createvendor', component: CreateVendorsComponent, canActivate: [AuthGuardService] },
  { path: 'vendorsdashboard', component: VendorsDashboardComponent, canActivate: [AuthGuardService] },
  { path: 'service-vendor/:id', component: ViewServiceVendorComponent, canActivate: [AuthGuardService] },
  { path: 'allservicerequest/:tab', component: SuperAdminServiceRequestsComponent, canActivate: [AuthGuardService] },
  { path: 'adminlist', component: AdminListComponent, canActivate: [AuthGuardService] },
  { path: 'view-admin/:id', component: ViewAdminComponent, canActivate: [AuthGuardService] },
  { path: 'view-channelpartner/:id', component: ViewChannelPartnerComponent, canActivate: [AuthGuardService] },
  { path: 'channelpartnerlist', component: ChannelPartnerListComponent, canActivate: [AuthGuardService] },
  { path: '', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
  providers: [AuthGuardService]
})
export class AppRoutingModule { }
