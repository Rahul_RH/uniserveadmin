import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FileValidator } from '../shared/validator/file-input.validator';
import { ServicesService, ServiceExpertsService } from '../shared';
import { Router } from '@angular/router';
import { UserService } from '../shared/services/user.service';
import{ToasterService} from'../shared/services/toaster-service.service';
//import { NavController } from 'ionic-angular';

@Component({
  selector: 'app-create-admin',
  templateUrl: './create-admin.component.html',
  styleUrls: ['./create-admin.component.css']
})
export class CreateAdminComponent implements OnInit {
  fieldTextType: boolean;
  public loading = false;
  public userProfileForm: FormGroup;
  public employeeProfile: any;
  todayDate: string;
  public expertServices: any = [];
  currentUserDetails: any;
  location = ['Bangalore', 'Delhi', 'Mumbai']
  constructor(private formBuilder: FormBuilder,
    private servicesService: ServicesService,
    private serviceExpertsService: ServiceExpertsService,
    private userService: UserService,
    private toasterService:ToasterService,
    private router: Router) { }

    
    Success()
    {this.toasterService.Success("Admin Created Successfully!!")}
    Error()
    {this.toasterService.Error("Something Went Wrong!!")}

  ngOnInit() {
    this.currentUserDetails = this.userService.getCurrentUser()

    this.todayDate = new Date().toISOString().substring(0, 10);
    this.userProfileForm = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')])],
      mobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^(0|[1-9][0-9]*)$')])],
      location: [null, Validators.required],
      password: [null, Validators.compose([Validators.required])]
    });


  }

  saveExpertProfile() {
    this.loading = true;
    let expertIn;
    let otherExpertizedFields;
    if (this.userProfileForm.value.expertIn == 'Other') {
      otherExpertizedFields = this.userProfileForm.value.otherExpertizedFields
      expertIn = this.userProfileForm.value.expertIn
    }
    else {
      expertIn = this.userProfileForm.value.expertIn
    }
    this.employeeProfile = {
      "contactDetails": {
        "email": this.userProfileForm.value.email,
        "mobileNumber": this.userProfileForm.value.mobileNumber,
      },
      "password": this.userProfileForm.value.password,
      'location': this.userProfileForm.value.location
    };
    this.serviceExpertsService.addAdmin(this.employeeProfile).subscribe(data => {
      this.loading = false;
      this.router.navigate(['superadmin']);
      this.Success();
    },
    error=>(this.Error())
    )
  }
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }

}
