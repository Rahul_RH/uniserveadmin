import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertsServiceReportComponent } from './experts-service-report.component';

describe('ExpertsServiceReportComponent', () => {
  let component: ExpertsServiceReportComponent;
  let fixture: ComponentFixture<ExpertsServiceReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertsServiceReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertsServiceReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
