import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ServicesService } from 'app/shared';

@Component({
  selector: 'app-experts-service-report',
  templateUrl: './experts-service-report.component.html',
  styleUrls: ['./experts-service-report.component.css']
})
export class ExpertsServiceReportComponent implements OnInit {
  expertId;
  expertReport = [];

  constructor(private router: Router,private route: ActivatedRoute,private servicesService: ServicesService) {
    this.expertId = this.route.snapshot.paramMap.get('id');
   }

  ngOnInit() {
    const dataObj = {"expertId": this.expertId};
    this.servicesService.getExpertReport(dataObj).subscribe((expertReport) =>{
    this.expertReport = expertReport;
    })
  }


}
