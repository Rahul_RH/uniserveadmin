import { Component, OnInit } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { ApiService, ServicesService, ServiceRequestService, ServiceExpertsService, UserService } from './shared';
import { Router } from '@angular/router';

declare const $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private ServieList: any;
  private serviceRequestOptions: any;
  private userRequests: any;
  private serviceExperts: any;
  private showComponent: boolean;

  constructor(public location: Location,
    private ApiService: ApiService,
    private servicesService: ServicesService,
    private ServiceRequestService: ServiceRequestService,
    private serviceExpertsService: ServiceExpertsService,
    private userService: UserService,
    private router: Router) { }
   
  ngOnInit() {
    console.log("Hi There!!!");
    this.userService.populate();
    $.material.options.autofill = true;
    $.material.init();

    this.servicesService.getRequestOptionsAPI().subscribe(data => {
      this.serviceRequestOptions = data;
    })
    this.userService.currentUser.subscribe(data => {
      if (data.usertype == "superadmin") {
        this.router.navigate(['superadmin'])
      } else {
        this.router.navigate(['servicerequests/new'])
      }
    })

  }

  isMaps(path) {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    titlee = titlee.slice(1);
    if (path == titlee) {
      return false;
    }
    else {
      return true;
    }
  }
}
