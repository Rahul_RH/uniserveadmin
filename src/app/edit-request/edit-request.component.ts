import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { ServiceExpertsService, ServiceRequestService, ServicesService } from 'app/shared';
import { Validators, FormBuilder, FormGroup, Validator } from '@angular/forms';
import { DatepickerOptions } from 'ng2-datepicker';
import * as frLocale from 'date-fns/locale/fr';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { UserService } from '../shared/services/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-edit-request',
  templateUrl: './edit-request.component.html',
  styleUrls: ['./edit-request.component.css']
})
export class EditRequestComponent implements OnInit {

  public requestID: any;
  public requsetedData: any = [];
  public requestDetails: any = [];
  public requestObj: any;
  public selectedObj;
  public key1: any;
  public key2: any;
  public key3: any; key4; key5; key6; key7; key8; key9;
  public filteredData: any = [];
  public createdData;
  public test: any;
  public expertServices: any = [];
  public allServiceExperts: any = [];
  public selectedCategoryExperts: any = [];
  public expertsDetailsHideAndShow: boolean = false;
  public expertsDetails: any;
  public status: any = ["Assigned", "Ongoing", "Completed", "Onhold", "Financepending"];
  public editExpert: boolean;
  public editDate: boolean;
  public editTime: boolean;
  public editNote = false;
  public editAmount = false;
  public loading = false;

  public updateServiceRequest: FormGroup;
  public updateServiceAmount: FormGroup;
  public updateServiceNote: FormGroup;
  public requestService: FormGroup;
  public RequestDate: Date;
  public RequestedTime: any;
  public requestNote: string;
  public requestTotalAmount: DoubleRange;
  private currentAdmin: any;
  getRequestSubscription: Subscription;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private serviceExpertsService: ServiceExpertsService,
    private serviceRequestService: ServiceRequestService,
    private servicesService: ServicesService,
    private atp: AmazingTimePickerService,
    private userService: UserService) {
    this.requestID = this.route.snapshot.paramMap.get('id');

    this.updateServiceRequest = this.formBuilder.group({
      status: ["", Validators.compose([Validators.required])]
    })

    this.requestService = this.formBuilder.group({
      category: [null, Validators.compose([Validators.required])],
      expertName: [null, Validators.compose([Validators.required])],
    })

    this.updateServiceAmount = this.formBuilder.group({
      amount: ["", Validators.compose([Validators.required, Validators.pattern('^[0-9]*(\.[0-9]+)?$')])]
    })
    this.updateServiceNote = this.formBuilder.group({
      note: ["", Validators.compose([Validators.required])]
    })

    this.requestService.controls['category'].valueChanges.subscribe((value) => {
      this.selectedCategoryExperts = [];
      this.expertsDetailsHideAndShow = true;
      this.expertsDetails = "";
      for (let i = 0; i < this.allServiceExperts.length; i++) {
        if (this.allServiceExperts[i].expertIn == value && this.allServiceExperts[i].availableStatus == 'Available') {
          this.selectedCategoryExperts.push((this.allServiceExperts[i]));
          // this.selectedCategoryExperts.push({'id':this.allServiceExperts[i].id, 'firstName': this.allServiceExperts[i].firstName, 'lastName': this.allServiceExperts[i].lastName})
        }
      }
    });

    this.requestService.controls['expertName'].valueChanges.subscribe((value) => {
      this.expertsDetailsHideAndShow = true;
      for (let i = 0; i < this.allServiceExperts.length; i++) {
        if (this.allServiceExperts[i].id == value) {
          this.expertsDetails = this.allServiceExperts[i];
        }
      }
    })

    this.editExpert = false;
    this.editDate = false;
    this.editTime = false;
  }

  ngOnInit() {

    this.userService.currentUser.subscribe(data => this.currentAdmin = data);
    // code of userRequestDetails
    this.getRequestSubscription = this.serviceRequestService.allUserRequest.subscribe((data: any) => {
      // console.log()
      if (this.requsetedData.length != data.length) {
        // console.log("test")
        this.requsetedData = data;
        this.requestObj = data;

        this.filteredData = [];
        for (let i = 0; i < this.requsetedData.length; i++) {
          if (this.requestID == this.requsetedData[i].id) {
            this.requestDetails = this.requsetedData[i];
            this.RequestDate = this.requsetedData[i].serviceRequestedDate;
            this.RequestedTime = this.requsetedData[i].serviceRequestedTime;
            this.requestNote = this.requsetedData[i].requestNote;
            this.requestTotalAmount = this.requsetedData[i].requestServiceAmount;
            this.updateServiceAmount.patchValue({ 'amount': this.requsetedData[i].requestServiceAmount })
            this.updateServiceNote.patchValue({ 'note': this.requsetedData[i].requestNote })
            this.updateServiceRequest.patchValue({'status' : this.requsetedData[i].requestStatus}) 
            this.createdData = new Date(this.requestObj[i].serviceRequestedDate).toISOString().substring(0, 10);
            this.key1 = Object.keys(this.requestObj[i].requestData);
            this.key2 = Object.keys(this.requestObj[i].requestData[this.key1]);
            this.key3 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2]);
            this.key4 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3]);

            for (let b = 0; b < this.key4.length; b++) {
              let keyvls1 = this.key4[b];

              this.key5 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1]);

              for (let l = 0; l < this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5].length; l++) {
                if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l] !== undefined) {
                  if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l].checked == true) {
                    this.selectedObj = this.requestObj[i].requestData[this.key1][this.key2][this.key3];
                    this.filteredData.push({ "KeyValue": keyvls1, "optionValue": this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l].option });

                    if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"]) {
                      this.key6 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"]);
                      for (let c = 0; c < this.key6.length; c++) {
                        let keyvls2 = this.key6[c];
                        this.key7 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2]);

                        for (let j = 0; j < this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7].length; j++) {
                          if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j] !== undefined) {
                            if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j].checked == true) {
                              this.filteredData.push({ "KeyValue": keyvls2, "optionValue": this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j].option });


                              if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"]) {
                                this.key8 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"]);
                                for (let a = 0; a < this.key8.length; a++) {
                                  let keyvls = this.key8[a];
                                  this.key9 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls]);


                                  for (let k = 0; k < this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9].length; k++) {
                                    if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k] !== undefined) {
                                      if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k].checked == true) {
                                        this.filteredData.push({ "KeyValue": keyvls, "optionValue": this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k].option });
                                      }
                                    }


                                  }
                                }


                              }
                            }
                          }
                        }


                      }
                    }


                  }
                }
              }
            }

          }
        }
      }

    })

    // code of category services starts from here  

    this.servicesService.currentService.subscribe(serviceData => {
      for (let key in serviceData.success[0]) {
        if (key != 'createdAt' && key != 'updatedAt' && key != 'id') {
          for (let i = 0; i < serviceData.success[0][key].length; i++) {
            this.expertServices.push(serviceData.success[0][key][i]);
          }
        }
      }
    })

    // code of service Experts starts from here

    this.serviceExpertsService.allServiceEmployeeData.subscribe(data => {
      this.allServiceExperts = data;
    })
  }

  updateRequestStatus() {
    this.loading = true;
    let objData = {
      'status': this.updateServiceRequest.value.status,
      'requestId': this.requestID,
      'admin_id': this.currentAdmin.id
    }
    this.serviceRequestService.updateRequestStatus(objData).subscribe(data => {
      this.loading = false;
      this.router.navigate(['/servicerequests/' + this.updateServiceRequest.value.status])
    })

  }

  updateExpert(data) {
    this.loading = true;
    let objData = {
      'expertData': data,
      'requestId': this.requestID,
      'expertId': this.requestDetails.expertDetails.id,
      'admin_id': this.currentAdmin.id
    }
    console.log(objData)
    this.serviceRequestService.updateRequestExpert(objData).subscribe(data => {
      if (data.success) {
        this.editExpert = false;
        this.loading = false;
        // this.updateAllChanges(data.userRequestedData);
        this.router.navigate(['/servicerequests/' + this.updateServiceRequest.value.status])
        // this.router.navigate(['/dashboard'])
      }
    })
  }

  open() {
    const amazingTimePicker = this.atp.open({
      time: this.RequestedTime,
      theme: 'dark',
      arrowStyle: {
        background: 'red',
        color: 'white'
      }
    });
    amazingTimePicker.afterClose().subscribe(time => {
      this.RequestedTime = time;
    });
  }
  updateDate() {
    this.loading = true;
    let objData = {
      'serviceRequestedDate': this.RequestDate,
      'serviceRequestedTime': this.RequestedTime,
      'requestId': this.requestID,
      'admin_id': this.currentAdmin.id
    }
    this.serviceRequestService.updateRequestTimings(objData).subscribe(data => {
      if (data.success) {
        this.editDate = false;
        this.editTime = false;
        this.loading = false;
        // this.updateAllChanges(data);
        // this.router.navigate(['/dashboard'])
        this.router.navigate(['/servicerequests/' + this.updateServiceRequest.value.status])
      }
    })
  }
  updateServiceNotes() {
    this.loading = true;
    let objData = {
      'note': this.updateServiceNote.value.note,
      'requestId': this.requestID,
      'admin_id': this.currentAdmin.id
    }
    this.serviceRequestService.updateRequestNote(objData).subscribe(data => {
      if (data.success) {
        this.editNote = false;
        this.loading = false;
        // this.updateAllChanges(data);
        // this.router.navigate(['/dashboard'])
        this.router.navigate(['/servicerequests/' + this.updateServiceRequest.value.status])
      }
    })
  }

  updateServiceAmounts() {
    this.loading = true;
    let objData = {
      'amount': this.updateServiceAmount.value.amount,
      'requestId': this.requestID,
      'admin_id': this.currentAdmin.id
    }
    console.log(objData);
    this.serviceRequestService.updateRequestAmount(objData).subscribe(data => {
      if (data.success) {
        this.editAmount = false;
        this.loading = false;
        // this.updateAllChanges(data);
        // this.router.navigate(['/dashboard'])
        this.router.navigate(['/servicerequests/' + this.updateServiceRequest.value.status])
      }
    })
  }

  ChangeExpert() {
    this.editExpert = true;
  }

  ChangeDate() {
    if (this.editDate == false) {
      this.editDate = true;
    }
    else {
      this.editDate = false;
    }
  }

  ChangeTime() {
    if (this.editTime == false) {
      this.editTime = true;
    }
    else {
      this.editTime = false;
    }
  }
  ChangeNote() {
    if (this.editNote == false) {
      this.editNote = true;
    }
    else {
      this.editNote = false;
    }
  }
  ChangeAmount() {
    if (this.editAmount == false) {
      this.editAmount = true;
    }
    else {
      this.editAmount = false;
    }
  }


  updateAllChanges(data) {

    this.editDate = false;
    this.editTime = false;
    this.requsetedData = data;
    this.requestObj = data;
    for (let i = 0; i < this.requsetedData.length; i++) {
      if (this.requestID == this.requsetedData[i].id) {
        this.requestDetails = this.requsetedData[i];
        this.RequestDate = this.requsetedData[i].serviceRequestedDate;
        this.RequestedTime = this.requsetedData[i].serviceRequestedTime;
        console.log(this.RequestedTime);
        this.createdData = new Date(this.requestObj[i].serviceRequestedDate).toISOString().substring(0, 10);
        this.key1 = Object.keys(this.requestObj[i].requestData);
        this.key2 = Object.keys(this.requestObj[i].requestData[this.key1]);
        this.key3 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2]);
        this.key4 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3]);

        for (let b = 0; b < this.key4.length; b++) {
          let keyvls1 = this.key4[b];

          this.key5 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1]);

          for (let l = 0; l < this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5].length; l++) {
            if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l] !== undefined) {
              if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l].checked == true) {
                this.selectedObj = this.requestObj[i].requestData[this.key1][this.key2][this.key3];
                this.filteredData.push({ "KeyValue": keyvls1, "optionValue": this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l].option });

                if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"]) {
                  this.key6 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"]);
                  for (let c = 0; c < this.key6.length; c++) {
                    let keyvls2 = this.key6[c];
                    this.key7 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2]);

                    for (let j = 0; j < this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7].length; j++) {
                      if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j] !== undefined) {
                        if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j].checked == true) {
                          this.filteredData.push({ "KeyValue": keyvls2, "optionValue": this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j].option });


                          if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"]) {
                            this.key8 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"]);
                            for (let a = 0; a < this.key8.length; a++) {
                              let keyvls = this.key8[a];
                              this.key9 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls]);


                              for (let k = 0; k < this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9].length; k++) {
                                if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k] !== undefined) {
                                  if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k].checked == true) {
                                    this.filteredData.push({ "KeyValue": keyvls, "optionValue": this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k].option });
                                  }
                                }


                              }
                            }


                          }
                        }
                      }
                    }


                  }
                }


              }
            }
          }
        }

      }
    }

  }

  cancelChangeExpert() {
    this.editExpert = ! this.editExpert
  }
  cancelChangeNote() {
    this.editNote = !this.editNote
  }
  cancelChangeAmount() {
    this.editAmount = !this.editAmount
  }
  cancelChangeTime() {
    this.editTime = !this.editTime
  }
  cancelChangeDate() {
    this.editDate = !this.editDate
  }

  ngOnDestroy() {
    this.getRequestSubscription.unsubscribe()
  }

}
