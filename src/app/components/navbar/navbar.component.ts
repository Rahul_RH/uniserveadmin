import { Component, OnInit, ElementRef } from '@angular/core';
import { ROUTES } from '../sidebar/sidebar.component';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { UserService } from '../../shared/services/user.service';
import { User } from '../../shared/models/user.model';
import { Router } from '@angular/router';
import { JwtService } from '../../shared/services/jwt.service';
@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
    private listTitles: any[];
    location: Location;
    private toggleButton: any;
    private sidebarVisible: boolean;
    public isAuth: boolean;
    public userDetails: User;
    currentUserDetails: any;
    constructor(location: Location, private element: ElementRef, private userService: UserService, private jwtService: JwtService, private router: Router) {
        this.location = location;
        this.sidebarVisible = false;
        this.userService.isAuthenticated.subscribe((data: boolean) => {
            this.isAuth = data;
        })
        this.userService.currentUser.subscribe((data: User) => {
            this.userDetails = data;
        })
    }

    ngOnInit() {
        this.listTitles = ROUTES.filter(listTitle => listTitle);
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
    }
    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');

        this.sidebarVisible = true;
    };
    sidebarClose() {
        const body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };
    sidebarToggle() {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    };
    logOut() {
        const action = confirm("Are You Sure To Logout....?")
        if (action == true) {
            this.userService.purgeAuth()
            // this.jwtService.destroyToken();
            this.router.navigate(['']);


        }
    }

    getTitle() {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(2);
        }
        titlee = titlee.split('/').pop();

        for (var item = 0; item < this.listTitles.length; item++) {
            if (this.listTitles[item].path === titlee) {
                return this.listTitles[item].title;
            }
        }

        return this.currentUserDetails.location;
    }
}
