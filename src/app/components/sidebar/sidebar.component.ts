import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { Router } from '@angular/router';
declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: 'servicerequests/new', title: 'Dashboard', icon: 'dashboard', class: '' },
  { path: 'serviceemployeedashboard', title: 'Add Service Employee', icon: 'list', class: '' },
  { path: 'vendorsdashboard', title: 'Add Vendors', icon: 'list', class: '' },
  // { path: 'addexperts', title: 'Add Service Experts', icon: 'add', class: '' },
  // { path: 'servicerequests/new', title: 'Service Requests', icon: 'content_paste', class: '' },
  { path: 'report', title: 'Report', icon: 'report', class: '' },
  
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  public isAuth: boolean;
  isAdmin = false;
  constructor(private userService: UserService,
    private router: Router) {
    this.userService.isAuthenticated.subscribe((data: boolean) => {
      this.isAuth = data;
    })
    userService.currentUser.subscribe((data: any) => {
      if (data.usertype == "superadmin") {
        this.isAdmin = false
      } else {
        this.isAdmin = true
      }
    });
  }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);

  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };
}
