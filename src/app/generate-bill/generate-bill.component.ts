import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { ServiceExpertsService, ServiceRequestService, ServicesService,BillsService  } from 'app/shared';
import { Validators, FormBuilder, FormGroup, Validator } from '@angular/forms';

@Component({
  selector: 'app-generate-bill',
  templateUrl: './generate-bill.component.html',
  styleUrls: ['./generate-bill.component.css']
})
export class GenerateBillComponent implements OnInit {
  requestDetails: any;
  public requestID:any;
  public requsetedData: any;
  public requestObj: any;
  public billForm: FormGroup;
  userId;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private serviceExpertsService: ServiceExpertsService,
    private serviceRequestService: ServiceRequestService,
    private servicesService: ServicesService,
    private billsService: BillsService) {
    this.requestID = this.route.snapshot.paramMap.get('id');
    this.billForm = this.formBuilder.group({
      username: [null, Validators.compose([Validators.required])],
      requestid: [null, Validators.compose([Validators.required])],
      requestdescription: [null, Validators.compose([Validators.required])],
      expertcost: [null, Validators.compose([Validators.required])],
      CGST: [null, Validators.compose([Validators.required])],
      SGST: [null, Validators.compose([Validators.required])],
      totalcost: [null, Validators.compose([Validators.required])],
    });

        this.billForm.controls['expertcost'].valueChanges.subscribe((value) => {
      let tax = (parseFloat(value)*parseFloat("2.5"))/100;
      this.billForm.patchValue({CGST: tax});
      this.billForm.patchValue({SGST: tax});
      let total = parseFloat(value) + (2 * tax);
      this.billForm.patchValue({totalcost: total});  
    });
  }
  ngOnInit() {
    this.serviceRequestService.allUserRequest.subscribe(data => {
      this.requsetedData = data;
      this.requestObj = data;
      this.billsService.allBills.subscribe((bills: any) =>{
        const result = bills.filter(bill => {
          return bill.requestId == this.requestID
        });
            if(result.length > 0){
              this.userId = result[0].userId;
              if(result[0].userName){
                this.billForm.patchValue({username: result[0].userName});
                }
                this.billForm.patchValue({requestid: this.requestID});
                this.billForm.patchValue({requestdescription: result[0].description});
                  this.billForm.patchValue({expertcost: result[0].expertcost});
                  this.billForm.patchValue({CGST: result[0].CGST});
                  this.billForm.patchValue({SGST: result[0].SGST});
                  this.billForm.patchValue({totalcost: result[0].total});
            }
            else{
              for (let i = 0; i < this.requsetedData.length; i++) {
                if (this.requestID == this.requsetedData[i].id) {
                  this.requestDetails = this.requsetedData[i];
                  this.userId = this.requestDetails.userId;
                  if(this.requestDetails.userDetails){
                  this.billForm.patchValue({username: this.requestDetails.userDetails.contactDetails.name});
                  }
                  this.billForm.patchValue({requestid: this.requestID});
                  this.billForm.patchValue({requestdescription: Object.keys(this.requestObj[i].requestData)});
                  if(this.requestDetails.requestServiceAmount){
                    let tax = (parseFloat(this.requestDetails.requestServiceAmount)*parseFloat("2.5"))/100;
                    let expertcost = this.requestDetails.requestServiceAmount - (2 * tax) 
                    this.billForm.patchValue({CGST: tax});
                    this.billForm.patchValue({SGST: tax});
                    this.billForm.patchValue({expertcost: expertcost});
                    let total = parseFloat(this.requestDetails.requestServiceAmount);
                    this.billForm.patchValue({totalcost: total});
                  }
                }
              }
            }
      })
  })
  }

  generateBill(){
    let description = this.billForm.value.requestdescription;
    let billDetails = {
      "userId": this.userId,
      "userName": this.billForm.value.username, 
      "requestId": this.billForm.value.requestid, 
      "description": description[0],
      "expertcost": this.billForm.value.expertcost,
      "CGST": this.billForm.value.CGST,
      "SGST": this.billForm.value.SGST,
      "total": this.billForm.value.totalcost
    };

    this.billsService.generatebill(billDetails).subscribe((data) =>{
      this.router.navigate(['/servicerequests/new'])
    })
  }

}
