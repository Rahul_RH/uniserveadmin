import { Component, OnInit } from '@angular/core';
import { ServiceExpertsService, UserService } from '../shared/services';

@Component({
  selector: 'app-vendors-dashboard',
  templateUrl: './vendors-dashboard.component.html',
  styleUrls: ['./vendors-dashboard.component.css']
})
export class VendorsDashboardComponent implements OnInit {

  public allVendors: any = [];
  currentUserDetails: any;
  isAdmin = false
  constructor(private serviceExpertsService: ServiceExpertsService,
    private userService: UserService) { }

  ngOnInit() {
    this.currentUserDetails = this.userService.getCurrentUser()
    if (this.currentUserDetails.usertype == 'superadmin') {
      this.isAdmin = false
      this.serviceExpertsService.getAllVendorsSuperAdmin().subscribe(data => {

        this.allVendors = data.success;
      })
    } else {
      this.isAdmin = true
      this.serviceExpertsService.getAllVendors({ "admin_id": this.currentUserDetails.id, }).subscribe(data => {

        this.allVendors = data.success;
      })
    }

  }

}
