import { Component, OnInit } from '@angular/core';
import { ServiceExpertsService } from '../shared';

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.css']
})
export class AdminListComponent implements OnInit {

  allAdminList: any;
  constructor(private serviceExpertsService: ServiceExpertsService) { }

  ngOnInit() {
    this.serviceExpertsService.getAllAdmins().subscribe((response: any) => {
      this.allAdminList = response.success;
      console.log(this.allAdminList)
    })
  }

}
