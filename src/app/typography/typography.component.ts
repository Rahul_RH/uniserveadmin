import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { ServiceExpertsService, ServiceRequestService, ServicesService } from 'app/shared';
import { Validators, FormBuilder, FormGroup, Validator } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UserService } from '../shared/services/user.service';

@Component({
  selector: 'app-typography',
  templateUrl: './typography.component.html',
  styleUrls: ['./typography.component.css']
})
export class TypographyComponent {
  public requestID: any;
  public requsetedData: any = [];
  public requestDetails: any = [];
  public requestObj: any;
  public selectedObj;
  public key1: any;
  public key2: any;
  public key3: any; key4; key5; key6; key7; key8; key9; key10; key11;
  public filteredData: any = [];
  public createdData;
  public test: any;
  public expertServices: any = [];
  public allServiceExperts: any = [];
  public selectedCategoryExperts: any = [];
  public expertsDetailsHideAndShow: boolean = false;
  public expertsDetails: any;
  public loading = false;
  private currentAdmin: any;
  getRequestSubscription: Subscription;

  public requestService: FormGroup;


  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private serviceExpertsService: ServiceExpertsService,
    private serviceRequestService: ServiceRequestService,
    private servicesService: ServicesService,
    private userService: UserService) {
    this.requestID = this.route.snapshot.paramMap.get('id');

    this.requestService = this.formBuilder.group({
      category: [null, Validators.compose([Validators.required])],
      expertName: [null, Validators.compose([Validators.required])],
    })

    this.requestService.controls['category'].valueChanges.subscribe((value) => {
      this.selectedCategoryExperts = [];
      this.expertsDetailsHideAndShow = true;
      this.expertsDetails = "";
      for (let i = 0; i < this.allServiceExperts.length; i++) {
        if (this.allServiceExperts[i].expertIn == value && this.allServiceExperts[i].availableStatus == 'Available') {
          this.selectedCategoryExperts.push((this.allServiceExperts[i]));
          // this.selectedCategoryExperts.push({'id':this.allServiceExperts[i].id, 'firstName': this.allServiceExperts[i].firstName, 'lastName': this.allServiceExperts[i].lastName})
        }
      }
    });

    this.requestService.controls['expertName'].valueChanges.subscribe((value) => {
      this.expertsDetailsHideAndShow = true;
      for (let i = 0; i < this.allServiceExperts.length; i++) {
        if (this.allServiceExperts[i].id == value) {
          this.expertsDetails = this.allServiceExperts[i];
        }
      }
    })

  }

  ngOnInit() {

    // code of userRequestDetails
    this.userService.currentUser.subscribe(data => this.currentAdmin = data);
    this.getRequestSubscription = this.serviceRequestService.allUserRequest.subscribe(data => {
      this.requsetedData = data;
      this.requestObj = data;
      this.filteredData = []
      for (let i = 0; i < this.requsetedData.length; i++) {
        if (this.requestID == this.requsetedData[i].id) {
          this.requestDetails = this.requsetedData[i];


          this.createdData = new Date(this.requestObj[i].serviceRequestedDate).toISOString().substring(0, 10);
          this.key1 = Object.keys(this.requestObj[i].requestData);
          this.key2 = Object.keys(this.requestObj[i].requestData[this.key1]);
          this.key3 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2]);
          this.key4 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3]);

          for (let b = 0; b < this.key4.length; b++) {
            let keyvls1 = this.key4[b];

            this.key5 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1]);

            for (let l = 0; l < this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5].length; l++) {
              if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l] !== undefined) {
                if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l].checked == true) {
                  this.selectedObj = this.requestObj[i].requestData[this.key1][this.key2][this.key3];
                  this.filteredData.push({ "KeyValue": keyvls1, "optionValue": this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l].option });

                  if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"]) {
                    this.key6 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"]);
                    for (let c = 0; c < this.key6.length; c++) {
                      let keyvls2 = this.key6[c];
                      this.key7 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2]);

                      for (let j = 0; j < this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7].length; j++) {
                        if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j] !== undefined) {
                          if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j].checked == true) {
                            this.filteredData.push({ "KeyValue": keyvls2, "optionValue": this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j].option });


                            if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"]) {
                              this.key8 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"]);
                              for (let a = 0; a < this.key8.length; a++) {
                                let keyvls = this.key8[a];
                                this.key9 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls]);


                                for (let k = 0; k < this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9].length; k++) {
                                  if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k] !== undefined) {
                                    if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k].checked == true) {
                                      this.filteredData.push({ "KeyValue": keyvls, "optionValue": this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k].option });

                                      if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k]["data"]) {
                                        this.key10 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k]["data"]);
                                        for (let d = 0; d < this.key10.length; d++) {
                                          let keyvls3 = this.key10[d];
                                          this.key11 = Object.keys(this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k]["data"][keyvls3]);


                                          for (let m = 0; m < this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k]["data"][keyvls3][this.key11].length; m++) {
                                            if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k]["data"][keyvls3][this.key11][m] !== undefined) {
                                              if (this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k]["data"][keyvls3][this.key11][m].checked == true) {
                                                this.filteredData.push({ "KeyValue": keyvls3, "optionValue": this.requestObj[i].requestData[this.key1][this.key2][this.key3][keyvls1][this.key5][l]["data"][keyvls2][this.key7][j]["data"][keyvls][this.key9][k]["data"][keyvls3][this.key11][m].option });
                                              }
                                            }
                                          }
                                        }
                                      }

                                    }
                                  }


                                }
                              }


                            }
                          }
                        }
                      }


                    }
                  }


                }
              }
            }
          }

        }

      }
    })

    // code of category services starts from here  

    this.servicesService.currentService.subscribe(serviceData => {
      for (let key in serviceData.success[0]) {
        if (key != 'createdAt' && key != 'updatedAt' && key != 'id') {
          for (let i = 0; i < serviceData.success[0][key].length; i++) {
            this.expertServices.push(serviceData.success[0][key][i]);
          }
        }
      }
    })

    // code of service Experts starts from here

    this.serviceExpertsService.allServiceEmployeeData.subscribe(data => {
      this.allServiceExperts = data;
    })
  }

  assignExpert(id: any) {
    this.loading = true;
    for (let i = 0; i < this.allServiceExperts.length; i++) {
      if (this.allServiceExperts[i].id == id) {
        this.expertsDetails = this.allServiceExperts[i];
      }
    }

    let dataObj = {
      "requestId": this.requestID,
      "expertDetails": this.expertsDetails,
      'admin_id': this.currentAdmin.id
    };
    this.serviceRequestService.assignServiceExpert(dataObj).subscribe(data => {
      this.loading = false;
      this.router.navigate(['/servicerequests/assigned'])
    })

  }

  ngOnDestroy() {
    this.getRequestSubscription.unsubscribe()
  }

}
