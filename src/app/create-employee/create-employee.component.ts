import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FileValidator } from '../shared/validator/file-input.validator';
import { ServicesService, ServiceExpertsService } from '../shared';
import { Router } from '@angular/router';
import { UserService } from '../shared/services/user.service';
import{ToasterService} from'../shared/services/toaster-service.service';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  public loading = false;
  public userProfileForm: FormGroup;
  public employeeProfile: any;
  todayDate: string;
  public expertServices: any = [];
  currentUserDetails: any;
  constructor(private formBuilder: FormBuilder,
    private toasterService:ToasterService,
    private servicesService: ServicesService,
    private serviceExpertsService: ServiceExpertsService,
    private userService: UserService,
    private router: Router) { }
    
    Success()
    {this.toasterService.Success("Employee Data Added Successfully!!")}
    Error()
    {this.toasterService.Error("Something Went Wrong!!")}

  ngOnInit() {
    this.currentUserDetails = this.userService.getCurrentUser()

    this.todayDate = new Date().toISOString().substring(0, 10);
    this.userProfileForm = this.formBuilder.group({
      firstName: [null, Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z \-\']+')])],
      lastName: [null, Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z \-\']+')])],
      mobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^(0|[1-9][0-9]*)$')])],
      address: [null, Validators.compose([Validators.required])],
      expertIn: [null, Validators.compose([Validators.required])]
    });

    this.expertServices.push("Other")
    // this.serviceData = this.servicesService.getServices();
    this.servicesService.currentService.subscribe(serviceData => {
      for (let key in serviceData.success[0]) {
        if (key != 'createdAt' && key != 'updatedAt' && key != 'id') {
          for (let i = 0; i < serviceData.success[0][key].length; i++) {
            this.expertServices.push(serviceData.success[0][key][i]);
          }
        }
      }
    })
  }

  saveExpertProfile() {
    
    this.loading = true;
    let expertIn;
    let otherExpertizedFields;
    if (this.userProfileForm.value.expertIn == 'Other') {
      otherExpertizedFields = this.userProfileForm.value.otherExpertizedFields
      expertIn = this.userProfileForm.value.expertIn
    }
    else {
      expertIn = this.userProfileForm.value.expertIn
    }
    this.employeeProfile = {
      "firstName": this.userProfileForm.value.firstName,
      "lastName": this.userProfileForm.value.lastName,
      "mobileNumber": this.userProfileForm.value.mobileNumber,
      "address": this.userProfileForm.value.address,
      "expertIn": expertIn,
      "admin_id": this.currentUserDetails.id,
      "availableStatus": "Available"
    };
    // console.log("test", this.employeeProfile)
    this.serviceExpertsService.addServiceEmployee(this.employeeProfile).subscribe(data => {
      this.loading = false;
      this.router.navigate(['/serviceemployeedashboard']); 
      this.Success();
    },
    error=>(this.Error())
    )
  }

}
