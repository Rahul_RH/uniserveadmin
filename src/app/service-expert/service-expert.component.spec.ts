import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceExpertComponent } from './service-expert.component';

describe('ServiceExpertComponent', () => {
  let component: ServiceExpertComponent;
  let fixture: ComponentFixture<ServiceExpertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceExpertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceExpertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
