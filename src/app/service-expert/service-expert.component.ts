import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { ServiceExpertsService, ServiceRequestService, ServicesService } from 'app/shared';
import { Validators, FormBuilder, FormGroup, Validator } from '@angular/forms';
@Component({
  selector: 'app-service-expert',
  templateUrl: './service-expert.component.html',
  styleUrls: ['./service-expert.component.css']
})
export class ServiceExpertComponent implements OnInit {

  public allServiceExperts: any;
  public serviceExpertsDetails: any;
  public userProfileForm: FormGroup;
  public userProfileView: FormGroup;
  public userID: any;
  public userDetails: any;
  public dateOfBirth: any;
  public dateOfView: any;
  public Others: any = false;
  public expertServices: any = [];
  public expertsProfile: any;
  public expertStatus: any = [];
  public availableStatus: string;
  public toggleViewAndEdit = true;
  butDisabled = true;
  public expertsStatus: string;
  public loading = false;
  constructor(private formBuilder: FormBuilder,private route: ActivatedRoute,
    private router: Router, private serviceExpertsService: ServiceExpertsService,
     private serviceRequestService: ServiceRequestService, private servicesService: ServicesService) {
       
    this.userID = this.route.snapshot.paramMap.get('id');
    this.userProfileForm = this.formBuilder.group({
      firstName: [null, Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z]+$')])],
      lastName: [null, Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z]+$')])],
      mobileNumber: [null, Validators.compose([Validators.required,Validators.minLength(10),Validators.maxLength(10),Validators.pattern('^(0|[1-9][0-9]*)$')])],
      dateOfBirth: [null],
      address: [null, Validators.compose([Validators.required])],
      adharNumber: [null, Validators.compose([Validators.pattern('^(0|[1-9][0-9]*)$'),Validators.minLength(12),Validators.maxLength(12)])],
      expertIn: [null, Validators.compose([Validators.required])],
      availableStatus: [null, Validators.compose([Validators.required])],
      workExperience: [null,Validators.compose([Validators.pattern('^[0-9]+(\.[0-9]{1,2})?$')])],
      additionalInfo: [null],
      gender: ["Male"],
      // file: [""],
      otherExpertizedFields: [null]
      // file: ["", Validators.compose([FileValidator.validate])]
    });

    this.userProfileView = this.formBuilder.group({
      firstName: [null, Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z]+$')])],
      lastName: [null, Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z]+$')])],
      mobileNumber: [null, Validators.compose([Validators.required,Validators.minLength(10),Validators.maxLength(10),Validators.pattern('^(0|[1-9][0-9]*)$')])],
      dateOfBirth: [null],
      address: [null, Validators.compose([Validators.required])],
      adharNumber: [null, Validators.compose([Validators.pattern('^(0|[1-9][0-9]*)$'),Validators.minLength(12),Validators.maxLength(12)])],
      expertIn: [null, Validators.compose([Validators.required])],
      availableStatus: [null, Validators.compose([Validators.required])],
      workExperience: [null,Validators.compose([Validators.pattern('^[0-9]+(\.[0-9]{1,2})?$')])],
      additionalInfo: [null],
      gender: ["Male"],
      // file: [""],
      otherExpertizedFields: [null]
      // file: ["", Validators.compose([FileValidator.validate])]
    });

    this.userProfileForm.controls['expertIn'].valueChanges.subscribe((value) => {
      if(value == 'Other'){
        this.Others = true;
      }
      else{
        this.Others = false;
      }
    });
  }

  ngOnInit() {
    this.serviceExpertsService.allServiceExpertsData.subscribe(data =>{
      this.allServiceExperts = data;
      for(let i = 0; i< this.allServiceExperts.length; i++){
        if(this.userID == this.allServiceExperts[i].id){
          this.serviceExpertsDetails = this.allServiceExperts[i];
          this.expertsStatus = this.allServiceExperts[i].availableStatus;
          this.dateOfBirth = this.allServiceExperts[i].dateOfBirth;
          this.availableStatus = this.allServiceExperts[i].availableStatus;
          this.userProfileForm.patchValue({firstName: this.allServiceExperts[i].firstName});
          this.userProfileForm.patchValue({lastName: this.allServiceExperts[i].lastName});
          this.userProfileForm.patchValue({mobileNumber: this.allServiceExperts[i].mobileNumber});
          this.userProfileForm.patchValue({address: this.allServiceExperts[i].address});
          this.userProfileForm.patchValue({adharNumber: this.allServiceExperts[i].adharNumber});
          this.userProfileForm.patchValue({workExperience: this.allServiceExperts[i].workExperience});
          this.userProfileForm.patchValue({additionalInfo: this.allServiceExperts[i].additionalInfo});
          this.userProfileForm.patchValue({gender: this.allServiceExperts[i].gender});
          this.userProfileForm.patchValue({expertIn: this.allServiceExperts[i].expertIn});
          this.userProfileForm.patchValue({availableStatus: this.allServiceExperts[i].availableStatus});
          if(this.allServiceExperts[i].expertIn == 'Other'){
            this.Others = "true";
            this.userProfileForm.patchValue({otherExpertizedFields: this.allServiceExperts[i].otherExpertizedFields});
          }


          this.dateOfView = this.allServiceExperts[i].dateOfBirth;
          this.userProfileView.patchValue({firstName: this.allServiceExperts[i].firstName})
          this.userProfileView.patchValue({lastName: this.allServiceExperts[i].lastName});
          this.userProfileView.patchValue({mobileNumber: this.allServiceExperts[i].mobileNumber});
          this.userProfileView.patchValue({address: this.allServiceExperts[i].address});
          this.userProfileView.patchValue({adharNumber: this.allServiceExperts[i].adharNumber});
          this.userProfileView.patchValue({workExperience: this.allServiceExperts[i].workExperience});
          this.userProfileView.patchValue({additionalInfo: this.allServiceExperts[i].additionalInfo});
          this.userProfileView.patchValue({gender: this.allServiceExperts[i].gender});
          this.userProfileView.patchValue({expertIn: this.allServiceExperts[i].expertIn});
          this.userProfileView.patchValue({availableStatus: this.allServiceExperts[i].availableStatus});
          if(this.allServiceExperts[i].expertIn == 'Other'){
            this.Others = "true";
            this.userProfileView.patchValue({otherExpertizedFields: this.allServiceExperts[i].otherExpertizedFields});
          }
        }
      }
    })

    this.expertServices.push("Other")
    // this.serviceData = this.servicesService.getServices();
    this.servicesService.currentService.subscribe(serviceData =>{
      for(let key in serviceData.success[0]){
        if( key != 'createdAt' && key != 'updatedAt' && key != 'id'){
          for(let i = 0; i < serviceData.success[0][key].length; i++){
            this.expertServices.push(serviceData.success[0][key][i]);
          }
        }
      }
    })
    this.expertStatus.push("Available","Assigned");
  }

  updateExpertProfile(){
    this.loading = true;
    let expertIn;
    let otherExpertizedFields;
    if(this.userProfileForm.value.expertIn == 'Other'){
      otherExpertizedFields = this.userProfileForm.value.otherExpertizedFields
      expertIn = this.userProfileForm.value.expertIn
    }
    else{
      expertIn = this.userProfileForm.value.expertIn
      otherExpertizedFields = ""
    }
    this.expertsProfile = {
      "expertId": this.userID,
      "firstName": this.userProfileForm.value.firstName, 
      "lastName": this.userProfileForm.value.lastName, 
      "mobileNumber": this.userProfileForm.value.mobileNumber,
      "dateOfBirth": this.dateOfBirth,
      "gender": this.userProfileForm.value.gender,
      "address": this.userProfileForm.value.address,
      "adharNumber": this.userProfileForm.value.adharNumber,
      "expertIn": expertIn,
      "workExperience": this.userProfileForm.value.workExperience,
      "additionalInfo": this.userProfileForm.value.additionalInfo,
      "otherExpertizedFields": otherExpertizedFields,
      "availableStatus": this.userProfileForm.value.availableStatus,
    };

    this.serviceExpertsService.updateServiceExperts( this.expertsProfile).subscribe(data => {
      this.loading = false;
      this.router.navigate(['/serviceexpertdashboard']);
    })
  }

  EditMode(){
    if(this.toggleViewAndEdit == true){
      this.toggleViewAndEdit = false;
    }
    else{
      this.toggleViewAndEdit = true;
    }
  }

  delete(){
    this.loading = true;
    var expertsData = {"id": this.userID}
    if(this.expertsStatus == 'Available'){
      this.serviceExpertsService.deleteServiceExperts(expertsData).subscribe(data => {
        this.loading = false;
        this.router.navigate(['/serviceexpertdashboard']);
      })
    }
    else{
      this.loading = false;
      alert("can not delete the profilr")
    }
  }

  cancel(){
    this.router.navigate(['/serviceexpertdashboard']);
  }

}
