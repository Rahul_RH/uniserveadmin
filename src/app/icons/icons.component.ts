import { Component, OnInit } from '@angular/core';
// import { MyDateRangePickerModule } from 'mydaterangepicker';
import { ServiceRequestService } from 'app/shared';

@Component({
  selector: 'app-icons',
  templateUrl: './icons.component.html',
  styleUrls: ['./icons.component.css']
})

export class IconsComponent implements OnInit {
public requsetedData: any;
public reqData: any = [];
  constructor(private serviceRequestService: ServiceRequestService) {
   }

  ngOnInit() {

    this.serviceRequestService.allUserRequest.subscribe(data =>{
      this.requsetedData = data;
      for(let i = 0; i < this.requsetedData.length ; i++){
        if(this.requsetedData[i].requestStatus){
          // if(this.requsetedData[i].requestStatus != "New"){
            this.reqData.push(this.requsetedData[i]);
          // }
        }
      }
  })
  }

}
