import { Component, OnInit } from '@angular/core';
import { UserService, ServiceRequestService } from '../shared';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
// import 'rxjs/observable/interval';
import { interval } from 'rxjs/observable/interval';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-super-admin-service-requests',
  templateUrl: './super-admin-service-requests.component.html',
  styleUrls: ['./super-admin-service-requests.component.css']
})
export class SuperAdminServiceRequestsComponent implements OnInit {

  public newReq: any = [];
  public Assigned: any = [];
  public onGoingReq: any = []
  public complatedReq: any = [];
  public onHoldReq: any = [];
  public onFinancePendingReq: any = [];
  public onCancellReq: any = [];
  public requsetedData: any;
  public loading = false;
  tabOpen;
  constructor(private UserService: UserService, private route: ActivatedRoute, private ServiceRequestService: ServiceRequestService, private router: Router) {

  }

  ngOnInit() {
    this.tabOpen = this.route.snapshot.paramMap.get('tab');

    if (this.tabOpen.toLowerCase() == 'new') {
      document.getElementById('new').classList.add('active');
      document.getElementById('new#').classList.add('active');
    }
    else if (this.tabOpen.toLowerCase() == 'assigned') {
      document.getElementById('assigned').classList.add('active');
      document.getElementById('assigned#').classList.add('active');
    }
    else if (this.tabOpen.toLowerCase() == 'ongoing') {
      document.getElementById('ongoing').classList.add('active');
      document.getElementById('ongoing#').classList.add('active');
    }
    else if (this.tabOpen.toLowerCase() == 'onhold') {
      document.getElementById('onhold').classList.add('active');
      document.getElementById('onhold#').classList.add('active');
    }
    else if (this.tabOpen.toLowerCase() == 'financePending') {
      document.getElementById('financePending').classList.add('active');
      document.getElementById('financePending#').classList.add('active');
    }
    else if (this.tabOpen.toLowerCase() == 'completed') {
      document.getElementById('completed').classList.add('active');
      document.getElementById('completed#').classList.add('active');
    }
    // let j = 0;
    // Observable.interval(1000).subscribe((v) => {
    //   console.info('j: ' + j++);
    // })

    this.getRequestList()
    // setInterval(() => this.getRequestList(), 10 * 1000)


  }

  getRequestList() {
    this.ServiceRequestService.fetchAllSuperAdminServiceRequest().subscribe(data => {
      console.log(data)
      this.newReq = [];
      this.Assigned = [];
      this.onGoingReq = [];
      this.complatedReq = [];
      this.onHoldReq = [];
      this.onFinancePendingReq = [];
      this.onCancellReq = [];
      this.requsetedData = data.success;
      for (let i = 0; i < this.requsetedData.length; i++) {
        if (this.requsetedData[i].requestStatus) {
          if (this.requsetedData[i].requestStatus == "New") {
            this.newReq.push(this.requsetedData[i]);
          }
          if (this.requsetedData[i].requestStatus == "Assigned") {
            this.Assigned.push(this.requsetedData[i]);
          }
          if (this.requsetedData[i].requestStatus == "Ongoing") {
            this.onGoingReq.push(this.requsetedData[i]);
          }
          if (this.requsetedData[i].requestStatus == "Completed") {
            this.complatedReq.push(this.requsetedData[i]);
          }
          if (this.requsetedData[i].requestStatus == "Onhold") {
            this.onHoldReq.push(this.requsetedData[i]);
          }
          if (this.requsetedData[i].requestStatus == "Financepending") {
            this.onFinancePendingReq.push(this.requsetedData[i]);
          }
          if (this.requsetedData[i].requestStatus == "Cancelled") {
            this.onCancellReq.push(this.requsetedData[i]);
          }
        }
      }
    })
  }


  assignServiceExpert(requestId: any) {
    this.router.navigate(['/assignexpert', requestId]);
  }

  editAssignedServiceExpert(requestId: any) {
    this.router.navigate(['/edit-request', requestId]);
  }

  cancelRequest(requestId: any, expertId: any) {
    this.loading = true;
    this.ServiceRequestService.cancelRequest({ 'id': requestId, "expertId": expertId }).subscribe(data => {
      this.loading = false;
      document.getElementById('new#').classList.remove('active');
      document.getElementById('assigned#').classList.remove('active');
      document.getElementById('ongoing#').classList.remove('active');
      document.getElementById('onhold#').classList.remove('active');
      document.getElementById('financePending#').classList.remove('active');
      document.getElementById('completed#').classList.remove('active');
      document.getElementById('new').classList.remove('active');
      document.getElementById('assigned').classList.remove('active');
      document.getElementById('ongoing').classList.remove('active');
      document.getElementById('onhold').classList.remove('active');
      document.getElementById('financePending').classList.remove('active');
      document.getElementById('completed').classList.remove('active');

      document.getElementById('Cancelled').classList.add('active');
      document.getElementById('Cancelled#').classList.add('active');
      // this.router.navigate(['/dashboard']);
    })
  }
  generateBill(requestId: any) {
    this.router.navigate(['/generatebill', requestId]);
  }

  switchtab() {
    document.getElementById('new#').classList.remove('active');
    document.getElementById('assigned#').classList.add('active');
    document.getElementById('new').classList.remove('active');
    document.getElementById('assigned').classList.add('active');
  }

}
