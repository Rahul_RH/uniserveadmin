import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperAdminServiceRequestsComponent } from './super-admin-service-requests.component';

describe('SuperAdminServiceRequestsComponent', () => {
  let component: SuperAdminServiceRequestsComponent;
  let fixture: ComponentFixture<SuperAdminServiceRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperAdminServiceRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperAdminServiceRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
