import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, Validator, ValidatorFn } from '@angular/forms';
import { FileValidator } from '../shared/validator/file-input.validator';
import { ServicesService, ServiceExpertsService } from '../shared';
import { Router } from '@angular/router';
import { UserService } from '../shared/services/user.service';
import { ToasterService } from '../shared/services/toaster-service.service';

@Component({
  selector: 'app-create-channel-partener',
  templateUrl: './create-channel-partener.component.html',
  styleUrls: ['./create-channel-partener.component.css']
})
export class CreateChannelPartenerComponent implements OnInit {

  public loading = false;
  public userProfileForm: FormGroup;
  public channelPartenerProfile: any;
  todayDate: string;
  public expertServices: any = [];
  currentUserDetails: any;
  location = ['Bangalore', 'Delhi', 'Mumbai']


  constructor(private formBuilder: FormBuilder,
    private servicesService: ServicesService,
    private serviceExpertsService: ServiceExpertsService,
    private userService: UserService,
    private toasterService: ToasterService,
    private router: Router) { }
  Success() { this.toasterService.Success("Channel Partner Created Successfully!!") }
  Error() { this.toasterService.Error("Something Went Wrong!!") }


  ngOnInit() {
    this.currentUserDetails = this.userService.getCurrentUser()

    this.todayDate = new Date().toISOString().substring(0, 10);
    this.userProfileForm = this.formBuilder.group({
      // check: this.formBuilder.group({
      name: [null, Validators.compose([Validators.required])],
      location: [null, Validators.compose([Validators.required])],

    });


  }

  saveExpertProfile() {
    this.loading = true;
    this.channelPartenerProfile = {
      "name": this.userProfileForm.value.name,
      "location": this.userProfileForm.value.location
    };
    this.serviceExpertsService.addChannelPartener(this.channelPartenerProfile).subscribe(data => {
      this.loading = false;
      this.router.navigate(['superadmin'])
      this.Success();
    },
      error => (this.Error())
    )
  }

}
