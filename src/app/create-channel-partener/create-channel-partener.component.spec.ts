import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateChannelPartenerComponent } from './create-channel-partener.component';

describe('CreateChannelPartenerComponent', () => {
  let component: CreateChannelPartenerComponent;
  let fixture: ComponentFixture<CreateChannelPartenerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateChannelPartenerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateChannelPartenerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
