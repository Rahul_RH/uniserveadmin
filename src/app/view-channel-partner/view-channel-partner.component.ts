import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService, ServiceExpertsService } from '../shared';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-view-channel-partner',
  templateUrl: './view-channel-partner.component.html',
  styleUrls: ['./view-channel-partner.component.css']
})
export class ViewChannelPartnerComponent implements OnInit {

  allCP = [];
  public serviceExpertsDetails: any;
  public userProfileForm: FormGroup;
  public userProfileView: FormGroup;
  public userID: any;
  public userDetails: any;
  public dateOfBirth: any;
  public dateOfView: any;
  public Others: any = false;
  public expertServices: any = [];
  public expertsProfile: any;

  public toggleViewAndEdit = true;
  butDisabled = true;
  public expertsStatus: string;
  public loading = false;
  private currentAdmin: any;
  location = ['Bangalore', 'Delhi', 'Mumbai']

  constructor(private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private serviceExpertsService: ServiceExpertsService,
    private formBuilder: FormBuilder) {
    this.userID = this.route.snapshot.paramMap.get('id');

    this.userProfileForm = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required])],
      location: [null, Validators.compose([Validators.required])]
    });

    this.userProfileView = this.formBuilder.group({
      name: [null, Validators.compose([Validators.required])],
      location: [null, Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    this.userService.currentUser.subscribe(data => this.currentAdmin = data);
    this.serviceExpertsService.getAllChannelPartener().subscribe(data => {
      this.allCP = data.success
      for (let i = 0; i < this.allCP.length; i++) {
        if (this.userID == this.allCP[i].id) {
          this.serviceExpertsDetails = this.allCP[i];

          this.userProfileForm.patchValue({ name: this.allCP[i].name });
          this.userProfileForm.patchValue({ location: this.allCP[i].place });

          this.userProfileView.patchValue({ name: this.allCP[i].name });
          this.userProfileView.patchValue({ location: this.allCP[i].place });

        }
      }

    })
  }

  EditMode() {
    if (this.toggleViewAndEdit == true) {
      this.toggleViewAndEdit = false;
    }
    else {
      this.toggleViewAndEdit = true;
    }
  }

  delete() {

  }

  updateAdminProfile() {
  }

  cancel() {
    this.router.navigate(['/channelpartnerlist']);
  }


}
