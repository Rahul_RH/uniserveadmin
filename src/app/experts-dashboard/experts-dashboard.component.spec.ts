import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertsDashboardComponent } from './experts-dashboard.component';

describe('ExpertsDashboardComponent', () => {
  let component: ExpertsDashboardComponent;
  let fixture: ComponentFixture<ExpertsDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertsDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertsDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
