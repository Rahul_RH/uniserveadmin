import { Component, OnInit } from '@angular/core';
import { ServiceExpertsService } from 'app/shared';

@Component({
  selector: 'app-experts-dashboard',
  templateUrl: './experts-dashboard.component.html',
  styleUrls: ['./experts-dashboard.component.css']
})
export class ExpertsDashboardComponent implements OnInit {
  public allServiceExperts: any = [];
  constructor(private serviceExpertsService: ServiceExpertsService) { }

  ngOnInit() {
    this.serviceExpertsService.allServiceExpertsData.subscribe(data => {
      this.allServiceExperts = data;
    })
  }

}
