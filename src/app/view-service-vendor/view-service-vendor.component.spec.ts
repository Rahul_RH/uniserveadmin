import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewServiceVendorComponent } from './view-service-vendor.component';

describe('ViewServiceVendorComponent', () => {
  let component: ViewServiceVendorComponent;
  let fixture: ComponentFixture<ViewServiceVendorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewServiceVendorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewServiceVendorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
