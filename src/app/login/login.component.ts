import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/services/user.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { User } from '../shared/models/user.model';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import{ToasterService}from '../shared/services/toaster-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  fieldTextType: boolean;
  public userLogin: FormGroup;
  public userDetails: any;
  constructor(
    private formBuilder: FormBuilder, 
    private userService: UserService, 
    private router: Router,
    private toasterService:ToasterService,) {

    this.userLogin = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
  }
  
    Error()
    {this.toasterService.Error("Username OR Password is wrong ")}
    Error1()
    {this.toasterService.Info("Wrong Password")}

  ngOnInit() {
  }
  userLoginSubmit() {
    this.userService.login(this.userLogin.value).subscribe((data: any) => {
      if (data.success) {
        if (data.success.usertype == "superadmin") {
          this.router.navigate(['superadmin'])
        } else {
          this.userService.populate();
          this.router.navigate(['/servicerequests/new'])
        }
      }
      else {       
        this.Error();
        // alert("failure");
      }
    })
  }
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
}
