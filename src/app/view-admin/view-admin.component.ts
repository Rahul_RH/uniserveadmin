import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService, ServiceRequestService, ServiceExpertsService } from '../shared';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-view-admin',
  templateUrl: './view-admin.component.html',
  styleUrls: ['./view-admin.component.css']
})
export class ViewAdminComponent implements OnInit {

  allAdmin = [];
  public serviceExpertsDetails: any;
  public userProfileForm: FormGroup;
  public userProfileView: FormGroup;
  public userID: any;
  public userDetails: any;
  public dateOfBirth: any;
  public dateOfView: any;
  public Others: any = false;
  public expertServices: any = [];
  public expertsProfile: any;
  public expertStatus: any = [];
  public availableStatus: string;
  public toggleViewAndEdit = true;
  butDisabled = true;
  public expertsStatus: string;
  public loading = false;
  private currentAdmin: any;
  location = ['Bangalore', 'Delhi', 'Mumbai']

  constructor(private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private serviceExpertsService: ServiceExpertsService,
    private formBuilder: FormBuilder) {
    this.userID = this.route.snapshot.paramMap.get('id');

    this.userProfileForm = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required])],
      mobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^(0|[1-9][0-9]*)$')])],
      location: [null, Validators.required],
      // password: [null, Validators.compose([Validators.required])]
    });

    this.userProfileView = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required])],
      mobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^(0|[1-9][0-9]*)$')])],
      location: [null, Validators.required],
      // password: [null, Validators.compose([Validators.required])]
    });

  }

  ngOnInit() {
    this.userService.currentUser.subscribe(data => this.currentAdmin = data);
    this.serviceExpertsService.getAllAdmins().subscribe(data => {
      this.allAdmin = data.success
      for (let i = 0; i < this.allAdmin.length; i++) {
        if (this.userID == this.allAdmin[i].id) {
          console.log(this.allAdmin[i])
          this.serviceExpertsDetails = this.allAdmin[i];
          this.expertsStatus = this.allAdmin[i].availableStatus;
          this.availableStatus = this.allAdmin[i].availableStatus;
          this.userProfileForm.patchValue({ email: this.allAdmin[i].contactDetails.email });
          this.userProfileForm.patchValue({ mobileNumber: this.allAdmin[i].contactDetails.mobileNumber });
          this.userProfileForm.patchValue({ location: this.allAdmin[i].location });
          // this.userProfileForm.patchValue({ password: this.allAdmin[i].password });

          this.userProfileView.patchValue({ email: this.allAdmin[i].contactDetails.email });
          this.userProfileView.patchValue({ mobileNumber: this.allAdmin[i].contactDetails.mobileNumber });
          this.userProfileView.patchValue({ location: this.allAdmin[i].location });
          // this.userProfileView.patchValue({ password: this.allAdmin[i].password });
        }
      }

    })
  }
  EditMode() {
    if (this.toggleViewAndEdit == true) {
      this.toggleViewAndEdit = false;
    }
    else {
      this.toggleViewAndEdit = true;
    }
  }

  delete() {
    // this.loading = true;
    // var expertsData = { "id": this.userID, 'admin_id': this.currentAdmin.id }
    // if (this.expertsStatus == 'Available') {
    //   // this.serviceExpertsService.deleteServiceEmployee(expertsData).subscribe(data => {
    //   //   this.loading = false;
    //   //   this.router.navigate(['/adminlist']);
    //   // })
    // }
    // else {
    //   // this.loading = false;
    //   // alert("can not delete the profile")
    // }
  }

  updateAdminProfile() {
  }

  cancel() {
    this.router.navigate(['/adminlist']);
  }

}
