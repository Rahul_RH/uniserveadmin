import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, Validator } from '@angular/forms';
import { ServicesService } from 'app/shared';
import { FileValidator } from '../shared/validator/file-input.validator';
import { ServiceExpertsService } from '../shared/services/service-experts.service';
import { Router } from "@angular/router";
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  public userProfileForm: FormGroup;
  // private firstName: string = "";
  // private lastName: string = "";
  // private mobile: number;
  public todayDate: any;
  public files: any ;
  public expertsProfile: any;
  public ecodedFile: any = [];
  public Others: any = false;
  public loading = false;
  // private address: string = "";
  // private adharNumber: string = ""; 
  // private ExpertIn: string = "";
  // private workExperience: DoubleRange;
  // private additionalInfo: string = "";
  // private gender: string = "";
  public serviceData: any;
  public expertServices: any = [];
  // ,Validators.pattern('^[0-9]{5}(?:-[0-9]{4})?$')
  constructor(private formBuilder: FormBuilder,private servicesService: ServicesService,private serviceExpertsService: ServiceExpertsService,private router: Router) {
    this.todayDate = new Date().toISOString().substring(0, 10);
    
    this.userProfileForm = this.formBuilder.group({
      firstName: [null, Validators.compose([Validators.required])],
      lastName: [null, Validators.compose([Validators.required])],
      mobileNumber: [null, Validators.compose([Validators.required,Validators.minLength(10),Validators.maxLength(10),Validators.pattern('^(0|[1-9][0-9]*)$')])],
      dateOfBirth: [null],
      address: [null, Validators.compose([Validators.required])],
      adharNumber: [null, Validators.compose([Validators.pattern('^(0|[1-9][0-9]*)$'),Validators.minLength(12),Validators.maxLength(12)])],
      expertIn: [null, Validators.compose([Validators.required])],
      workExperience: [null,Validators.compose([Validators.pattern('^[0-9]+(\.[0-9]{1,2})?$')])],
      additionalInfo: [null],
      gender: ["Male"],
      otherExpertizedFields: [null],
      file: ["", Validators.compose([FileValidator.validate])]
    });

    this.userProfileForm.controls['expertIn'].valueChanges.subscribe((value) => {
      if(value == 'Other'){
        this.Others = true;
      }
      else{
        this.Others = false;
      }
    });

   }

  ngOnInit() {
    this.expertServices.push("Other")
    // this.serviceData = this.servicesService.getServices();
    this.servicesService.currentService.subscribe(serviceData =>{
      for(let key in serviceData.success[0]){
        if( key != 'createdAt' && key != 'updatedAt' && key != 'id'){
          for(let i = 0; i < serviceData.success[0][key].length; i++){
            this.expertServices.push(serviceData.success[0][key][i]);
          }
        }
      }
    })
  }

  saveExpertProfile(){
    this.loading = true;
    let expertIn;
    let otherExpertizedFields;
    if(this.userProfileForm.value.expertIn == 'Other'){
      otherExpertizedFields = this.userProfileForm.value.otherExpertizedFields
      expertIn = this.userProfileForm.value.expertIn
    }
    else{
      expertIn = this.userProfileForm.value.expertIn
    }
    this.expertsProfile = {
      "firstName": this.userProfileForm.value.firstName, 
      "lastName": this.userProfileForm.value.lastName, 
      "mobileNumber": this.userProfileForm.value.mobileNumber,
      "dateOfBirth": this.todayDate,
      "gender": this.userProfileForm.value.gender,
      "address": this.userProfileForm.value.address,
      "adharNumber": this.userProfileForm.value.adharNumber,
      "expertIn": expertIn,
      "workExperience": this.userProfileForm.value.workExperience,
      "additionalInfo": this.userProfileForm.value.additionalInfo,
      "files":  this.ecodedFile,
      "otherExpertizedFields": otherExpertizedFields,
      "availableStatus": "Available"
    };

    this.serviceExpertsService.addServiceExperts( this.expertsProfile).subscribe(data => {
      this.loading = false;
      this.router.navigate(['/dashboard']);
    })

  }

  fileChange(event) {
    this.ecodedFile = [];
    this.files = event.srcElement.files;
    if(this.files && this.files.length > 0) {
      for(let i = 0; i < this.files.length; i++){
        let reader = new FileReader();
        reader.readAsDataURL(this.files[i]);
        reader.onload = () => {
          this.ecodedFile.push(reader.result)
        };
      }
    }
}

}
